
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  

<div class="row">
	<div class="col-xl-12 mx-auto">
		<!-- <h6 class="mb-0 text-uppercase">Horizontal Form</h6>
		<hr/> -->

		<span id="error"></span>

		<form method="post" id="insert_form">
		<div class="card border-top border-0 border-4 border-info">
			<div class="card-body">
				<div class="border p-4 rounded">
					<div class="card-title d-flex align-items-center">
						<div><i class="bx bxs-user me-1 font-22 text-info"></i>
						</div>
						<h4 class="mb-0 text-info">Item Master</h4>
					</div>
					<hr/> 

					<div class="row mb-3">
						<label for="inputEnterYourName" class="col-sm-3 col-form-label">Item Code</label>
						<div class="col-sm-9">
							<input class="result form-control" name="icod"  placeholder="Item Code..." required>
						</div>
					</div>
					<div class="row mb-3">
            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Item Name</label>
            <div class="col-sm-9">
              <input class="result form-control" name="iname"  placeholder="Item Name..." required>
            </div>
          </div>

          <div class="row mb-3">
            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Misno</label>
            <div class="col-sm-9">
              <input class="result form-control" name="misno"  placeholder="Misno...">
            </div>
          </div>

          <div class="row mb-3">
            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Igroup</label>
            <div class="col-sm-9">
              <input class="result form-control" name="igroup"  placeholder="Igroup..." required>
            </div>
          </div>
					 
        <div class="table-repsonsive">
          
         

          <div align="center">
            <input type="submit" name="submit" class="btn btn-info sbutton" value="Submit" />
            <!--<input type="submit" name="submit" class="btn btn-warning" value="Submit" />-->
          </div>
        </div>
      </form>
    </div> 

 
				</div>
			</div>
		</div>
	</div>
</div>

 <script type="text/javascript">
    $('#insert_form').on('submit', function(event){
        event.preventDefault();
        var error = '';
         

        var form_data = $(this).serialize();

        if(error == '')
        {
          $(".sbutton").attr('disabled', 'disabled');
          $.ajax({
            url:"<?php echo base_url('saveItemMaster'); ?>",
            method:"POST",
            data:form_data,
            success:function(data)
            {
              if(data == 'ok')
              { 
                $('#error').html('<div class="alert alert-success">Data Saved Successfully...</div>');
                $('#insert_form').trigger("reset");
                $(".sbutton").removeAttr('disabled');
              }
            }
          });
        }
        else
        {
          $('#error').html('<div class="alert alert-danger">'+error+'</div>');
        }

      }); 
 </script>