<div class="row">
   
        <!-- <h6 class="mb-0 text-uppercase">Horizontal Form</h6>
        <hr/> -->
        
        <div class="card border-top border-0 border-4 border-info">
            <div class="card-body">
                <div class="border p-4 rounded">
                    <div class="card-title d-flex align-items-center">
                        <div><i class="bx bxs-user me-1 font-22 text-info"></i>
                        </div>
                        <h5 class="mb-0 text-info">Update Profile</h5>
                    </div>
                    <hr/> 
                    <div class="row">
                     <div class="col-md-8">
                      <form method="post" action="<?php echo base_url('profileSubmit'); ?>"  enctype="multipart/form-data">

                         <strong><?php if($this->session->flashdata('message')!=''): ?>
                           <div class="success_message alert alert-success">
                           <?php  echo $this->session->flashdata('message'); ?>
                           </div><?php
                            endif; ?></strong></h1>


                         <div class="col-md-12 mb-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">First Name</label>
                          <input type="text" required name="firstName" placeholder="First Name" class="form-control" value="<?php echo $userInfo[0]['first_name']; ?>">
                        </div>
                      </div>                     
                   
                         <div class="col-md-12 mb-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text" required name="lastName" placeholder="Last Name" class="form-control" value="<?php echo $userInfo[0]['last_name']; ?>">
                        </div>
                      </div>                     
                     

                    
                         <div class="col-md-12 mb-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input type="text" name="email" placeholder="email" class="form-control" value="<?php echo $userInfo[0]['email']; ?>" readonly>
                        </div>
                      </div>                     
                   
                         <div class="col-md-12 mb-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Mobile</label>
                          <input type="text" readonly name="mobile" placeholder="mobile" class="form-control" value="<?php echo $userInfo[0]['mobile']; ?>">
                        </div>
                      </div> 

                     <div class="col-md-12 mb-3">
                        <div class="form-group">
                          <label class="bmd-label-floating"><?php if($user_type==1): echo "Upload Logo"; else: echo "Profile Pic"; endif;?></label>
                          <input type="file" name="profilePic" placeholder="Profile Picture" class="form-control">
                        </div>
                      </div>                     
                     
                
                  
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>


                 <div class="col-md-4 col-xl-4">
              <div class="card card-profile">
                <div class="card-avatar">
                  <a href="#pablo">
                    <?php if(empty($userInfo[0]['profilePic'])){
                      $profilePic = base_url()."assets/"."noimage.png";
                    }else{
                      $profilePic = base_url()."assets/profilePics/".$userInfo[0]['profilePic'];
                    } ?>
                    <img class="img" src="<?php echo $profilePic; ?>" style="width: 100%;"/>
                  </a>
                </div> 
              </div>
            </div>

              </div>
            </div>

           
          </div> 
     
    
      <script>
       $(".interested_section").hide();
       $(".rejection_section").hide();
       $(".chkConfirmedYes_section").hide();
       $(".chkConfirmedNo_section").hide();
         $(function () {
             
        $("#chkInterested").click(function () {
            if ($(this).is(":checked")) {
                $(".interested_section").show();
                $("#chkRejected").attr("disabled", true);
                $(".rejected_reason").removeAttr("required");
            } else {
                $(".interested_section").hide();
                $("#chkRejected").removeAttr("disabled");
            }
        });
        
        $("#chkRejected").click(function () {
            if ($(this).is(":checked")) {
                $("#chkInterested").attr("disabled", true);
                 $(".next_appointment").removeAttr("required");
                  $(".status").removeAttr("required");
                $(".rejection_section").show();
            } else {
                $(".rejection_section").hide();
                $("#chkInterested").removeAttr("disabled");
            }
        });
        
        
        //Confirmed Script
        
         $("#chkConfirmedYes").click(function () {
            if ($(this).is(":checked")) {
                $(".chkConfirmedYes_section").show();
                $("#chkConfirmedNo").attr("disabled", true);
                $(".not_confirmed_reason").removeAttr("required");
            } else {
                $(".chkConfirmedYes_section").hide();
                $("#chkConfirmedNo").removeAttr("disabled");
            }
        });
        
        $("#chkConfirmedNo").click(function () {
            if ($(this).is(":checked")) {
                $("#chkConfirmedYes").attr("disabled", true);
                // $(".next_appointment").removeAttr("required");
                  $(".confirmed_closing_status").removeAttr("required");
                $(".chkConfirmedNo_section").show();
            } else {
                $(".chkConfirmedNo_section").hide();
                $("#chkConfirmedYes").removeAttr("disabled");
            }
        });
        
        //end
        
    });
      </script>