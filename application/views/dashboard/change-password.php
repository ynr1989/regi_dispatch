

<div class="row">
  <div class="col-xl-12 mx-auto">
    <!-- <h6 class="mb-0 text-uppercase">Horizontal Form</h6>
    <hr/> -->

    <span id="error"></span>
 
    <div class="card border-top border-0 border-4 border-info">
      <div class="card-body">
        <div class="">
          <div class="card-title d-flex align-items-center">
            <div>
              <i class="bx bxs-user me-1 font-22 text-info"></i>
            </div>
            <h4 class="mb-0 text-info">Change Password</h4> 
          </div>
          <hr/>   

                     <form method="post" action="<?php echo base_url('submitChangePassword'); ?>">

                    <div class="row mb-3">
                        <strong><?php  if($this->session->flashdata('message2')!=''): ?>
                            <div class="alert alert-success">
                           <?php echo $this->session->flashdata('message2'); ?>
                           </div>
                            <?php  endif; 

                            if($this->session->flashdata('message1')!=''): ?>
                           <div class="alert alert-danger">
                            <?php echo $this->session->flashdata('message1'); ?>
                            </div><?php
                            endif; ?>
                          </h1>
                    </div>

                    <?php //if($this->session->flashdata('messageStatus')!='success'): ?>
                    

                    <div class="row mb-3">
                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">New Password</label>
                        <div class="col-sm-9">
                           <input type="password" required name="password" placeholder="Password" class="form-control">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Confirm Password</label>
                        <div class="col-sm-9">
                             <input type="password" required name="confirm_password" placeholder="Confirm Password" class="form-control">
                        </div>
                    </div>

                    <div class="col-12">
                         <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                    </form>
                    <?php //endif; ?>
                    
                </div>
               
              


              </div>
            </div>
             </div>
            </div>
             </div>
            </div>
            <!-- <div class="col-md-3">
              <div class="card card-profile">
                <div class="card-avatar">
                  <a href="#pablo">
                    <img class="img" src="../assets/img/marc.jpg" />
                  </a>
                </div>
                <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">Alec Thompson</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      
    
      <script>
       $(".interested_section").hide();
       $(".rejection_section").hide();
       $(".chkConfirmedYes_section").hide();
       $(".chkConfirmedNo_section").hide();
         $(function () {
             
        $("#chkInterested").click(function () {
            if ($(this).is(":checked")) {
                $(".interested_section").show();
                $("#chkRejected").attr("disabled", true);
                $(".rejected_reason").removeAttr("required");
            } else {
                $(".interested_section").hide();
                $("#chkRejected").removeAttr("disabled");
            }
        });
        
        $("#chkRejected").click(function () {
            if ($(this).is(":checked")) {
                $("#chkInterested").attr("disabled", true);
                 $(".next_appointment").removeAttr("required");
                  $(".status").removeAttr("required");
                $(".rejection_section").show();
            } else {
                $(".rejection_section").hide();
                $("#chkInterested").removeAttr("disabled");
            }
        });
        
        
        //Confirmed Script
        
         $("#chkConfirmedYes").click(function () {
            if ($(this).is(":checked")) {
                $(".chkConfirmedYes_section").show();
                $("#chkConfirmedNo").attr("disabled", true);
                $(".not_confirmed_reason").removeAttr("required");
            } else {
                $(".chkConfirmedYes_section").hide();
                $("#chkConfirmedNo").removeAttr("disabled");
            }
        });
        
        $("#chkConfirmedNo").click(function () {
            if ($(this).is(":checked")) {
                $("#chkConfirmedYes").attr("disabled", true);
                // $(".next_appointment").removeAttr("required");
                  $(".confirmed_closing_status").removeAttr("required");
                $(".chkConfirmedNo_section").show();
            } else {
                $(".chkConfirmedNo_section").hide();
                $("#chkConfirmedYes").removeAttr("disabled");
            }
        });
        
        //end
        
    });
      </script>