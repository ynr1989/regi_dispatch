<!--start page wrapper -->
		 
		<link href="<?php echo base_url(); ?>assets/plugins/highcharts/css/highcharts.css" rel="stylesheet" />
			
			  <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-4">
			  	<?php
			  	$array=$trafficitems=[]; 
			  	$milk_db = $milk_dashboard->MilkDashBoard;
			  	$itemcodes_str=$itemQty_str=$itemTubs_str= "";
			  	$itemQty_total = 0;
			  	for($i=0; $i<=count($milk_db);$i++){
			  		$bgcolor = "bg-danger";
			  		if($i%2==0){
			  			$bgcolor = "bg-primary";
			  		}
			  		if($milk_db[$i]->TotalQty>0):

	  			$array["name"] = $milk_db[$i]->ItemCode." - ".(float) $this->mainModel->oneDecimals($milk_db[$i]->TotalQty);
	  			$array["y"] = (float) $this->mainModel->oneDecimals($milk_db[$i]->TotalQty);
	  			$array["z"] = (float) $this->mainModel->noDecimals($milk_db[$i]->TotalTubs); 
	  			$trafficitems[] = $array;

	  			$itemcodes_str .= ",".sprintf('"%s"',$milk_db[$i]->ItemCode);
	  			$itemQty_str .= ",". (float)$this->mainModel->oneDecimals($milk_db[$i]->TotalQty);
	  			$itemTubs_str .= ",". (float)$this->mainModel->noDecimals($milk_db[$i]->TotalTubs);
	  			$itemQty_total+= (float)$this->mainModel->oneDecimals($milk_db[$i]->TotalQty);
			  	?>
			    <div class="col">
						<div class="card radius-10 overflow-hidden <?php echo $this->mainModel->cardBgColor($i); ?>">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div> 
										<p class="mb-0 text-white"><?php echo $milk_db[$i]->ItemCode; ?></p>
										<h5 class="mb-0 text-white">Qty: <?php echo $this->mainModel->oneDecimals($milk_db[$i]->TotalQty); ?>  </h5>
										<h5 class="mb-0 text-white">Tubs: <?php echo $this->mainModel->noDecimals($milk_db[$i]->TotalTubs); ?> </h5>
									</div>
									<div class="ms-auto text-white">	<i class='bx bx-cart font-30'></i>
									</div>
								</div>
							</div>
							<!-- <div class="" id="chart1"></div> -->
						</div>
					</div>
				<?php endif; }
				$trafficitems = json_encode($trafficitems);
				$target_reached = (($itemQty_total/375000)*100);
				$target_reached = $this->mainModel->noDecimals($target_reached); 
				if($itemcodes_str){
					$itemcodes_str = substr($itemcodes_str,1);
				}
				if($itemQty_str){
					$itemQty_str = substr($itemQty_str,1);
				}
				if($itemTubs_str){
					$itemTubs_str = substr($itemTubs_str,1);
				}
				?>
 
					<!-- <div class="col">
						<div class="card radius-10 overflow-hidden bg-primary">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<p class="mb-0 text-white">Total Income</p>
										<h5 class="mb-0 text-white">$52,945</h5>
									</div>
									<div class="ms-auto text-white">	<i class='bx bx-wallet font-30'></i>
									</div>
								</div>
							</div>
							<div class="" id="chart2"></div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10 overflow-hidden bg-warning">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<p class="mb-0 text-dark">Total Users</p>
										<h5 class="mb-0 text-dark">24.5K</h5>
									</div>
									<div class="ms-auto text-dark">	<i class='bx bx-group font-30'></i>
									</div>
								</div>
							</div>
							<div class="" id="chart3"></div>
						</div>
					</div>
					<div class="col">
						<div class="card radius-10 overflow-hidden bg-success">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<p class="mb-0 text-white">Comments</p>
										<h5 class="mb-0 text-white">869</h5>
									</div>
									<div class="ms-auto text-white">	<i class='bx bx-chat font-30'></i>
									</div>
								</div>
							</div>
							<div class="" id="chart4"></div>
						</div>
					</div> -->
			  </div><!--end row-->
			  
			  
			  <div class="row">
			   <!--  <div class="col-12 col-xl-8 d-flex">
				  <div class="card radius-10 w-100">
						<div class="card-body">
							<div class="" id="chart5"></div>
						</div>
					</div>
				</div> -->

				 <div class="col-12 col-xl-8 d-flex">
				  <div class="card radius-10 w-100">
						<div class="card-body">
							<div class="" id="trafficitems"></div>
						</div>
					</div>
				</div>

				<div class="col-12 col-xl-4 d-flex">
				  <div class="card radius-10 w-100">
						<div class="card-body">
							<div class="d-flex align-items-center">
									<div>
										<h5 class="mb-1">Sales Target</h5>
									</div>
									<div class="font-22 ms-auto"><i class="bx bx-dots-horizontal-rounded"></i>
									</div>
								</div>
							<div class="mt-4" id="chart6"></div>
							<div class="d-flex align-items-center">
									<div>
										<h2 class="mb-0"><?php echo $itemQty_total; ?></h2>
										<p class="mb-0">/3,75,000 target</p>
									</div>
									<div class="ms-auto d-flex align-items-center border radius-10 px-2">
									  <i class='bx bxs-checkbox font-22 me-1 text-primary'></i><span>Marketing Sales</span>
									</div>
							  </div>
						</div>
					</div>
				</div>

				<div class="col-12 col-xl-12 d-flex">
				  <div class="card radius-10 w-100">
				  	<h6 class="mb-0 text-uppercase">Line Chart</h6> 
						<div class="chart-container1">
							<canvas id="linechart1"></canvas>
						</div>
				  </div>
				</div>

				<div class="col-12 col-xl-12 d-flex">
				  <div class="card radius-10 w-100">
				  	<h6 class="mb-0 text-uppercase">Bar Chart</h6> 
						<div class="chart-container1">
							<canvas id="barchart1"></canvas>
						</div>
				  </div>
				</div>

			  </div><!--end row-->

 
				 
				<div class="row">
					<div class="col">
						<div class="card radius-10 mb-0">
							<div class="card-body">
								<div class="d-flex align-items-center">
									<div>
										<h5 class="mb-1">Recent Orders</h5>
									</div>
									<!-- <div class="ms-auto">
										<a href="javscript:;" class="btn btn-primary btn-sm radius-30">View All Products</a>
									</div> -->
								</div>

                               <div class="table-responsive mt-3">
								   <table class="table align-middle mb-0">
									   <thead class="table-light">
										   <tr>
											   <th>S.NO</th>
											   <th>Products Name</th>
											   <th>Qty</th>
											   <th>Tubs</th> 
										   </tr>
									   </thead>
									   <tbody>
									   	<?php
									   	for($i=0; $i<=count($milk_db);$i++){
									   		?>
										   <tr>
											   <td><?php echo $i+1; ?></td>
											   <td>
												<div class="d-flex align-items-center">
													<!-- <div class="recent-product-img">
														<img src="" alt="">
													</div> -->
													<div class="ms-2">
														<h6 class="mb-1 font-14"><?php echo $milk_db[$i]->ItemCode; ?></h6>
													</div>
												</div>
											   </td>
											   <td><?php echo (float) $this->mainModel->oneDecimals($milk_db[$i]->TotalQty);?></td>
											   <td><?php echo (float) $this->mainModel->noDecimals($milk_db[$i]->TotalTubs); ?></td> 
										   </tr>
										    <?php }; ?>
									   </tbody>
								   </table>
							   </div>
								
							</div>
						</div>
					</div>
				</div><!--end row-->


	<script src="<?php echo base_url(); ?>assets/plugins/highcharts/js/highcharts.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/highcharts/js/exporting.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/highcharts/js/variable-pie.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/highcharts/js/export-data.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/highcharts/js/accessibility.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/apexcharts-bundle/js/apexcharts.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/chartjs/js/Chart.min.js"></script>

	<script type="text/javascript">
		Highcharts.chart('trafficitems', {
		chart: {
			type: 'variablepie',
			height: 330,
			styledMode: true
		},
		credits: {
			enabled: false
		},
		title: {
			text: 'Total Traffic by Source'
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		accessibility: {
			point: {
				valueSuffix: '%'
			}
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %'
				}
			}
		},
		series: [{
			minPointSize: 10,
			innerSize: '65%',
			zMin: 0,
			name: 'Qty',
			//data: [{name:"FCM-GOLD",y:505370.0,z:92},{name:"FCM-SPL",y:551500.5,z:118}] 
			data: <?php echo $trafficitems; ?>
		}]
	}); 
	var ctx = document.getElementById("barchart1").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: [<?php echo $itemcodes_str; ?>],
			datasets: [{
				label: 'Qty',
				data: [<?php echo $itemQty_str; ?>],
				barPercentage: .5,
				backgroundColor: "#0d6efd"
			}, {
				label: 'Tubs',
				data: [<?php echo $itemTubs_str; ?>],
				barPercentage: .5,
				backgroundColor: "#f41127"
			}]
		},
		options: {
			maintainAspectRatio: false,
			legend: {
				display: true,
				labels: {
					fontColor: '#585757',
					boxWidth: 40
				}
			},
			tooltips: {
				enabled: true
			},
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero: true,
						fontColor: '#585757'
					},
					gridLines: {
						display: true,
						color: "rgba(0, 0, 0, 0.07)"
					},
				}],
				yAxes: [{
					ticks: {
						beginAtZero: true,
						fontColor: '#585757'
					},
					gridLines: {
						display: true,
						color: "rgba(0, 0, 0, 0.07)"
					},
				}]
			}
		}
	});

	var ctx = document.getElementById('linechart1').getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: [<?php echo $itemcodes_str; ?>],
			datasets: [{
				label: 'Qty',
				data: [<?php echo $itemQty_str; ?>],
				backgroundColor: "transparent",
				borderColor: "#0d6efd",
				pointRadius: "0",
				borderWidth: 4
			}, {
				label: 'Tubs',
				data: [<?php echo $itemTubs_str; ?>],
				backgroundColor: "transparent",
				borderColor: "#f41127",
				pointRadius: "0",
				borderWidth: 4
			}]
		},
		options: {
			maintainAspectRatio: false,
			legend: {
				display: true,
				labels: {
					fontColor: '#585757',
					boxWidth: 40
				}
			},
			tooltips: {
				enabled: false
			},
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero: true,
						fontColor: '#585757'
					},
					gridLines: {
						display: true,
						color: "rgba(0, 0, 0, 0.07)"
					},
				}],
				yAxes: [{
					ticks: {
						beginAtZero: true,
						fontColor: '#585757'
					},
					gridLines: {
						display: true,
						color: "rgba(0, 0, 0, 0.07)"
					},
				}]
			}
		}
	});


	//Target graph
	// chart 6
	var options = {
		chart: {
			height: 300,
			type: 'radialBar',
			toolbar: {
				show: false
			}
		},
		plotOptions: {
			radialBar: {
				//startAngle: -135,
				//endAngle: 225,
				hollow: {
					margin: 0,
					size: '78%',
					//background: '#fff',
					image: undefined,
					imageOffsetX: 0,
					imageOffsetY: 0,
					position: 'front',
					dropShadow: {
						enabled: false,
						top: 3,
						left: 0,
						blur: 4,
						color: 'rgba(0, 169, 255, 0.25)',
						opacity: 0.65
					}
				},
				track: {
					background: '#dfecff',
					//strokeWidth: '67%',
					margin: 0, // margin is in pixels
					dropShadow: {
						enabled: false,
						top: -3,
						left: 0,
						blur: 4,
						color: 'rgba(0, 169, 255, 0.85)',
						opacity: 0.65
					}
				},
				dataLabels: {
					showOn: 'always',
					name: {
						offsetY: -25,
						show: true,
						color: '#6c757d',
						fontSize: '16px'
					},
					value: {
						formatter: function (val) {
							return val + "%";
						},
						color: '#000',
						fontSize: '45px',
						show: true,
						offsetY: 10,
					}
				}
			}
		},
		fill: {
			type: 'gradient',
			gradient: {
				shade: 'light',
				type: 'horizontal',
				shadeIntensity: 0.5,
				gradientToColors: ['#0d6efd'],
				inverseColors: false,
				opacityFrom: 1,
				opacityTo: 1,
				stops: [0, 100]
			}
		},
		colors: ["#0d6efd"],
		series: [<?php echo $target_reached; ?>],
		stroke: {
			lineCap: 'round',
			//dashArray: 4
		},
		labels: ['Reached'],
	}
	var chart = new ApexCharts(document.querySelector("#chart6"), options);
	chart.render();
	</script>