

  

<div class="row">
  <div class="col-xl-12 mx-auto">
    <!-- <h6 class="mb-0 text-uppercase">Horizontal Form</h6>
    <hr/> --> 

    <span id="error"></span>

    <form >
    <div class="card border-top border-0 border-4 border-info">
      <div class="card-body">
        <div class="border p-4 rounded">
          <div class="card-title d-flex align-items-center">
            <div>
              <i class="bx bxs-user me-1 font-22 text-info"></i>
            </div>
            <h4 class="mb-0 text-info">Milk Products Tubs</h4> 
          </div>
          <hr/>  
          <div class="col-md-6 mb-3">
            <label for="inputEnterYourName" class="col-sm-4 col-form-label">Date</label>
            <div class="col-sm-8">
               <input class="result form-control" required="required" name="rdate"  value="<?php if(isset($_GET['rdate'])){ echo $_GET['rdate']; }else{echo date('d-m-Y');} ?>" type="text" id="date" placeholder="Date Picker...">
            </div>
          </div>

           

                <div class="col-md-6 mb-3">
                  <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Shift</label>
                  <div class="col-sm-8">
                    <select name="shift" class="form-control" required="required">
                      <option value="">Select Shift</option>
                      <option value="AM" <?php if(isset($_GET['shift']) && $_GET['shift'] == 'AM'){ echo "selected"; } ?>>AM</option>
                      <option value="PM" <?php if(isset($_GET['shift']) && $_GET['shift'] == 'PM'){ echo "selected"; } ?>>PM</option>
                    </select>
                  </div>
                </div>

               

          <div class="col-md-6 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Route</label>
            <div class="col-sm-8"> 
              <?php
              $itemInfo = $this->mainModel->getDispatchRoutes('1');
              ?>
              <select name="route" class="multiple-select" data-placeholder="Choose anything" >
              <option value="">Select Route</option>
                <?php foreach($itemInfo as $itemInfos): ?>
                <option value="<?php echo $itemInfos['route']; ?>" <?php if(isset($_GET['route']) && $_GET['route'] == $itemInfos['route']){ echo "selected"; } ?>><?php echo $itemInfos['route']; ?></option>
                <?php endforeach; ?>
              </select>

            </div>
          </div> 
          
          <div class="col-md-6 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Vehicle Number</label>
            <div class="col-sm-8">
              <?php
              $itemInfo = $this->mainModel->getDispatchVehicles('1');
              ?>
              <select name="vehicle_no" class="multiple-select" data-placeholder="Choose anything" >
              <option value="">Select Vehicle</option>
                <?php foreach($itemInfo as $itemInfos): ?>
                <option value="<?php echo $itemInfos['vehicle_no']; ?>" <?php if(isset($_GET['vehicle_no']) && $_GET['vehicle_no'] == $itemInfos['vehicle_no']){ echo "selected"; } ?>><?php echo $itemInfos['vehicle_no']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div> 



          <div class="col-md-6 mb-3"> 
                  <input type="submit" name="submit" class="btn btn-info mb-3" value="Get Details" /> 
                  <a href="<?php echo base_url('milkdispatch'); ?>" class="btn btn-info mb-3">Reset</a>
              </div>
      </form>

        <?php if($this->session->flashdata('message')!=''): ?>
      <div class="smessage">
        <div class="col-md-12 mb-3"> 
        <div class="alert alert-success">
           <?php echo $this->session->flashdata('message'); ?>
         </div>
       </div>
     </div>
      <?php  endif; 

      if($this->session->flashdata('message1')!=''): ?>
        <div class="smessage">
          <div class="col-md-10 mb-3"> 
         <div class="alert alert-danger">
            <?php echo $this->session->flashdata('message1'); ?>
        </div>
      </div>
      </div>
        <?php
      endif; ?>

      <?php
      /*if(isset($_GET['submit'])){ 
        $mddata = array(
            "rdate" => $_GET['rdate'],
            "shift" => $_GET['shift'],
            "route" => $_GET['route'],
            "vehicle_no" => $_GET['vehicle_no']
        ); */
        //$checkCount = $this->mainModel->checkProductTubsRecordCount($mddata);
      //if(count($checkCount)>0){ ?>
        <!-- <div class="smessage">
            <div class="col-md-10 mb-3"> 
           <div class="alert alert-danger">
              Data already exist. If you want to continue ok...
          </div>
        </div>
        </div> -->
    <?php //} 

 // } ?>
   
     <form method="post" id="insert_form" action="<?php echo base_url('saveProductTubs'); ?>">
        <input type="hidden" name="mddata[rdate]" value="<?php if(isset($_GET['rdate'])){ echo $_GET['rdate']; }else{echo date('d-m-Y');} ?>">
        <input type="hidden" name="mddata[vehicle_no]" value="<?php if(isset($_GET['vehicle_no'])){ echo $_GET['vehicle_no']; } ?>">
        <input type="hidden" name="mddata[shift]" value="<?php if(isset($_GET['shift'])){ echo $_GET['shift']; } ?>">
        <input type="hidden" name="mddata[route]" value="<?php if(isset($_GET['route'])){ echo $_GET['route']; } ?>">
        
        <div class="table-repsonsive">
          
          <table class="table table-bordered table-repsonsive" id="item_table">
            <thead>
              <tr class="trsum"> <th rowspan="2">S.No</th>
                <th rowspan="2">Product Name</th>  
                <th colspan="1" class="text-center">Dispatch</th>
                <th colspan="1" class="text-center">Receipt</th>
                <th colspan="1" class="text-center">Difference</th>
               <th rowspan="2">Remarks</th>
                <th rowspan="2">Payment Status</th>
                <th rowspan="2">CRT Number</th>
              </tr>
              <tr class="trsum">                
                <th>Tubs</th>
                <!-- <th>Cans</th> --> 
         
                <th>Tubs</th>
                <!-- <th>Cans</th>  -->

                <th>Tubs</th>
                <!-- <th>Cans</th>  -->

                

              </tr>
            </thead>
            <tbody>
              <?php 
      if(isset($_GET['submit'])){
        $data = $this->mainModel->getProductDispatchItems($_GET['rdate'],$_GET['shift'],$_GET['route'],$_GET['vehicle_no']);
         $i=$dtubs=$rtubs=$dftubs=0;   
        foreach($data as $datas){
        $dtubs += $datas['dis_total_tubs'];
        $rtubs += $datas['dis_total_tubs'];
        $dftubs += $this->mainModel->numDifference($datas['dis_total_tubs'],$datas['dis_total_tubs']); ?>
          <input type="hidden" name="ditems[<?php echo $i; ?>][line_no]" value="<?php echo $i; ?>">
          <input type="hidden" name="ditems[<?php echo $i; ?>][item_code]" value="<?php echo $datas['item_code']; ?>">
         
         <input type="hidden" name="ditems[<?php echo $i; ?>][dis_tubs]" value="<?php echo number_format($datas['dis_total_tubs'], 0, '.', ''); ?>">
          <input type="hidden" name="ditems[<?php echo $i; ?>][dis_cans]" value="<?php echo number_format($datas['dis_total_cans'], 0, '.', ''); ?>"> 


          <tr><td><?php echo $i+1; ?></td>
            <!--<td><?php echo $datas['item_code']; ?></td> -->
            <td><?php echo $datas['iname']; ?></td> 
            <td><?php echo number_format($datas['dis_total_tubs'], 0, '.', ''); ?></td>
            <!-- <td><?php echo number_format($datas['dis_total_cans'], 0, '.', ''); ?></td> -->   

            <td><input type="text" name="ditems[<?php echo $i; ?>][rec_tubs]" 
              value="<?php echo number_format($datas['dis_total_tubs'], 0, '.', '') ?>" class="changeData rtubs" data-lineid="<?php echo $i; ?>" data-type="tubs" data-oldval="<?php echo number_format($datas['dis_total_tubs'], 0, '.', ''); ?>"></td> 
            <!-- <td><input type="text" name="ditems[<?php echo $i; ?>][rec_cans]" value="
              <?php echo number_format($datas['dis_total_cans'], 0, '.', '') ?>"></td>  -->

           <td><span class="tubs_<?php echo $i; ?>"><?php echo $this->mainModel->numDifference($datas['dis_total_tubs'],$datas['dis_total_tubs']); ?></span></td> 
          <!--   <td><?php echo $this->mainModel->numDifference($datas['dis_total_cans'],$datas['dis_total_cans']); ?></td>  -->

            <td><input type="text" name="ditems[<?php echo $i; ?>][remarks]" /></td>
            <td>
              <select name="ditems[<?php echo $i; ?>][payment_status]" class="form-control" >
                  <option value="">Select Status</option> 
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>

            </td>
            <td><input type="text" name="ditems[<?php echo $i; ?>][crt_number]" /></td>
             
              <input type="hidden" name="ditems[<?php echo $i; ?>][diff_tubs]" value="<?php echo $this->mainModel->numDifference($datas['dis_total_tubs'],$datas['dis_total_tubs']); ?>" class="tubs_<?php echo $i; ?> diff_tubs" >
              <input type="hidden" name="ditems[<?php echo $i; ?>][diff_cans]" value="<?php echo $this->mainModel->numDifference($datas['dis_total_cans'],$datas['dis_total_cans']); ?>"> 
          </tr> 
        <?php $i++; }
      }   
      ?>  
            </tbody>
            <tfoot>
              <tr class="qtyrow trsum"><th colspan="1"></th><th>Total</th><th><?php echo $dtubs ; ?></th><th><span class="rtubstotal"><?php echo $rtubs ; ?></span></th><th><span class="difftubstotal"><?php echo $dftubs ; ?></span></th><th colspan="3"></th></tr>
            </tfoot>
          </table>

          <div class="row mb-3">
            <label for="inputEnterYourName" class="col-sm-3 col-form-label">Remarks</label>
            <div class="col-sm-9">
              <textarea  name="mddata[remarks]" class="form-control" id="bremarks" placeholder="Enter Remarks"></textarea> 
            </div>
          </div>

          <div align="center">
            <input type="submit" name="submit" class="btn btn-info" value="Submit" />
            <!--<input type="submit" name="submit" class="btn btn-warning" value="Submit" />-->
          </div>
        </div>
      </form>
    </div>  
</div>
     
 
<script type="text/javascript">

$(".changeData").on('change', function() {
  var current_val = $(this).val();
  var lineid = $(this).data("lineid");
  var type = $(this).data("type");
  var oldval =  $(this).data("oldval");
  var final_val = parseFloat(oldval) -  parseFloat(current_val);

  console.log("final_val",final_val)
  if(final_val>0){
    $("."+type+"_"+lineid).html(final_val);
    $("."+type+"_"+lineid).val(final_val);
    console.log(current_val,lineid);
  }else{
    console.log(oldval , current_val)
    /*if(oldval == current_val || oldval < parseInt(current_val)){
      $("."+type+"_"+lineid).html("0")
    }*/

    $("."+type+"_"+lineid).html(final_val);
    $("."+type+"_"+lineid).val(final_val);

    if(final_val != 0){
      alert("Please check value once.");
    }
    
    //$("."+type+"_"+lineid).html("0");
    //$("."+type+"_"+lineid).val("0");
  }

  var sum1 = 0; 
  $(".rtubs").each(function(){
      sum1 += +$(this).val(); 
  }); 
  $(".rtubstotal").html(sum1);

  var sum2 = 0; 
  $(".diff_tubs").each(function(){
      sum2 += +$(this).val(); 
      console.log("sum2",sum2);
  }); 
  $(".difftubstotal").html(sum2);


});


//$(document).on("change", ".rtubs", function() {
 /*  $(".rtubs").each(function(){
      sum1 += +$(this).val(); 
  }); 
  $(".rtubstotal").html(sum1);*/
//}


</script>