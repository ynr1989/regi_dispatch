<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--favicon-->
	<link rel="icon" href="assets/images/kmu.jpg" type="image/png" />
	<!--plugins-->
	<link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
	<link href="assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
	<link href="assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
	<!-- loader-->
	<link href="<?php echo base_url(); ?>assets/css/pace.min.css" rel="stylesheet" />
	<script src="assets/js/pace.min.js"></script>
	<!-- Bootstrap CSS -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/app.css" rel="stylesheet">
	<link href="assets/css/icons.css" rel="stylesheet">
	<!-- Theme Style CSS -->
	<link rel="stylesheet" href="assets/css/dark-theme.css" />
	<link rel="stylesheet" href="assets/css/semi-dark.css" />
	<link rel="stylesheet" href="assets/css/header-colors.css" />
	<title>Regi Prod</title>
</head>

<body class="bg-login">
	<!--wrapper-->
	<div class="wrapper">
		<div class="section-authentication-signin d-flex align-items-center justify-content-center my-5 my-lg-0">
			<div class="container-fluid">
				<div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
					<div class="col mx-auto">
						<div class="mb-4 text-center">
							<img src="assets/images/kmu.png" width="200" alt="" />
							<!--<img src="assets/images/kmu_union.jpg" width="300" alt="" />-->
							
						</div>
						<div class="card">
							<div class="card-body">
								<div class="border p-4 rounded">
									 
									<div class="form-body">
										<form class="row g-3 loginForm" method="post">

									<div  class="login_popup_spinner spinnerNewBg" style="display:none;">
                <img height="50px" width="50px" src="<?php echo base_url();?>assets/images/timer.gif">
            </div>
            <div class="error_popup alert innerAlert alert-danger" style="display:none;"></div>
            <div class="success_popup alert innerAlert alert-success" style="display:none;"></div>

											<div class="col-12">
												<label for="inputEmailAddress" class="form-label">Enter Mobile</label>
												<input type="text"  class="form-control border-start-0" id="loginMobile" placeholder="Enter Mobile" id="inputEmailAddress"  >
											</div>
											<div class="col-12">
												<label for="inputChoosePassword" class="form-label">Enter Password</label>
												<div class="input-group" id="show_hide_password">
													<input type="password" class="form-control border-start-0" id="loginPassword" placeholder="Enter Password" > <a href="javascript:;" class="input-group-text bg-transparent"><i class='bx bx-hide'></i></a>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-check form-switch">
													<input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
													<label class="form-check-label" for="flexSwitchCheckChecked">Remember Me</label>
												</div>
											</div>
											<div class="col-md-6 text-end">	<a href="#">Forgot Password ?</a>
											</div>
											<div class="col-12">
												<div class="d-grid">
													<!--<button type="submit" class="btn btn-primary"><i class="bx bxs-lock-open"></i>Sign in</button>-->
													<!--<button type="submit" class="btn btn-success"><i class="bx bxs-lock-open"></i>Sign in</button>-->
													<button type="submit" class="btn btn-warning"><i class="bx bxs-lock-open"></i>Sign in</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--end row-->
			</div>
		</div>
	</div></div>
		 
		 
	</div>
	<!--end wrapper-->
	 
	<!-- Bootstrap JS -->
	<script src="assets/js/bootstrap.bundle.min.js"></script>
	<!--plugins-->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/plugins/simplebar/js/simplebar.min.js"></script>
	<script src="assets/plugins/metismenu/js/metisMenu.min.js"></script>
	<script src="assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
	<!--app JS-->
	<script src="assets/js/app.js"></script>
</body>



<script type="text/javascript">
    $(".loginForm").submit(function(){
    var emailMobile = $.trim($("#loginMobile").val());
    var password = $.trim($("#loginPassword").val());
    var error_msg = '';

    if(emailMobile == '')
    {
    error_msg = 'Please Enter Email/Mobile Number'; 
    }else if(password == '')
    {
    error_msg = 'Please Enter Password'; 
    }

   /* if(mobile.length <10 && mobile!="")
    {
    error_msg = 'Please enter valid mobile number';
    $('#myModal').modal({
      backdrop: 'static',
      keyboard: false
    });
    }*/

    if(error_msg != '')
    {
    $(".error_popup").show();
    $(".error_popup").html(error_msg);
    return false;
    }

    var qData = {
        emailMobile : emailMobile,
        password:password
    }

    $(".login_popup_spinner").show();
    $(".error_popup").hide();
    $.ajax({
    type: 'POST',
    url: '<?php echo base_url('loginAction'); ?>',
    data: qData,
    dataType : "text",
        success: function(resultData) {
            if(resultData == 'success')
            {            
                /*$("#myModal").modal('hide');
                $("#myModal-otp").modal('show');
                setTimeout(function() {
                    $(".resendotp").show();
                }, 3000);*/
                window.location.reload();
            }
            else
            {
                $(".login_popup_spinner").hide();
                $(".error_popup").show();
                $(".error_popup").html(resultData);
                return false;
            }
        }
    });
    return false;
    });

</script>


</html>