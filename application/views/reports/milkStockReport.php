
<div class="row">
  <div class="col-xl-12 mx-auto">
    <!-- <h6 class="mb-0 text-uppercase">Horizontal Form</h6>
    <hr/> -->

    <span id="error"></span>

    <form >
    <div class="card border-top border-0 border-4 border-info">
      <div class="card-body">

        <div class="">
          <div class="card-title d-flex align-items-center">
            <div><i class="bx bxs-user me-1 font-22 text-info"></i>
            </div>
            <h4 class="mb-0 text-info">Milk Products Issue Summarry Report</h4> 
          </div>
          <hr/>  

          <div class="col-md-3 mb-3">
            <label for="inputEnterYourName" class="col-sm-4 col-form-label">From Date</label>
            <div class="col-sm-8">
               <input class="result form-control" required="required" name="from_date"  value="<?php if(isset($_GET['from_date'])){ echo $_GET['from_date']; }else{echo date('d-m-Y');} ?>" type="text" id="date" placeholder="Date Picker...">
            </div>
          </div>

          <div class="col-md-3 mb-3">
            <label for="inputEnterYourName" class="col-sm-4 col-form-label">To Date</label>
            <div class="col-sm-8">
               <input class="result form-control" required="required" name="to_date"  value="<?php if(isset($_GET['to_date'])){ echo $_GET['to_date']; }else{echo date('d-m-Y');} ?>" type="text" id="to_date" placeholder="Date Picker...">
            </div>
          </div> 

          <div class="col-md-3 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Item</label>
            <div class="col-sm-8">
             <?php
              $itemInfo = $this->mainModel->getItemsMaster();
              ?>
              <select name="item_code" class="multiple-select" data-placeholder="Choose anything" >
              <option value="">Select Item</option>
                <?php foreach($itemInfo as $itemInfos): ?>
                <option value="<?php echo $itemInfos['icod']; ?>" <?php if(isset($_GET['item_code']) && $_GET['item_code'] == $itemInfos['icod']){ echo "selected"; } ?>><?php echo $itemInfos['iname']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div> 
           
          <div class="col-md-3 mb-3"> 
              <input type="submit" name="submit" class="btn btn-info mb-3" value="Get Details" /> 
              <a href="<?php echo base_url('milk-stock-report'); ?>" class="btn btn-info mb-3">Reset</a>
          </div>
      </form>
   
     </div></div></div>
        
        <div class="table-repsonsive">
          
         <table id="example2" class="table table-striped table-bordered"> 
            <thead>
              <tr class="trsum"> 
                <th>S.No</th>
                <th>Product Name</th>
                <th>OB</th>
                <th>Receipts</th>
                <th>Dispatches</th>
                <th>Issues</th>
                <th>CB</th> 
              </tr> 
            </thead>
            <tbody>
              <?php 
              $date_query = 0;
      if(isset($_GET['submit'])){
        $yesterday_date = date( 'Y-m-d', strtotime( $_GET['from_date'] . ' -1 day' ) );
        $where = " WHERE status>0 ";  
        if(!isset($_GET['from_date']) && !isset($_GET['to_date']) && $_GET['from_date']=="" && $_GET['to_date']=""){
          $tdate = date("Y-m-d");
          $where .=" AND rdate ='$tdate'";
          $date_query = 1;
        } 

        if(((isset($_GET['from_date']) && $_GET['from_date']!="" && $_GET['to_date']=="") || ($_GET['from_date'] == $_GET['to_date'])) && ($_GET['from_date']!="")){       
          $where.= " AND rdate='".$this->mainModel->dateFormatChange($_GET['from_date'],1)."'";
          $date_query = 1;
        }

        if(isset($_GET['to_date']) && $_GET['to_date']!="" && $_GET['from_date']==""){
          $where.= " AND rdate='".$this->mainModel->dateFormatChange($_GET['to_date'],1)."'";
          $date_query = 1;
        }   
 
        if(isset($_GET['to_date']) && $_GET['to_date']!="" && $_GET['from_date']!="" && $_GET['from_date'] != $_GET['to_date']){ 
          $fdate=$this->mainModel->dateFormatChange($_GET['from_date'],1); 
          $tdate = $this->mainModel->dateFormatChange($_GET['to_date'],1);
          $where.= "AND (rdate between '$fdate' and '$tdate' )";
          $date_query = 1;
        }

        if(isset($_GET['item_code']) && $_GET['item_code']!=""){
          $where.= " AND item_code='".$_GET['item_code']."'";
        }
         
        $sql = "SELECT * FROM item_master WHERE igroup = '101' AND status=1 ORDER BY misno";  
        $row = $this->mainModel->get_Result($sql); 
        $i=$tqty=$receipt_qty_sum=$dispatch_qty_sum=$issue_qty_sum=$yesterday_receipt_qty_sum=$yesterday_dispatch_qty_sum=$yesterday_issue_qty_sum=0;
        foreach($row as $datas){ 
        $iname = strtoupper($datas["iname"]);
        $item_id = $datas["id"];  

        $receipt_ids = $this->mainModel->get_Result_By_Column("SELECT GROUP_CONCAT(milk_receipt_id) as receipt_ids FROM `milk_receipt` $where","receipt_ids"); 
        if($receipt_ids){
          $receipt_qty_sum = $this->mainModel->get_Result_By_Column("SELECT SUM(qty) as tqty FROM `milk_receipt_items` WHERE milk_receipt_id IN($receipt_ids) AND item_id='$item_id'","tqty"); 
        }  

        $dispatch_ids = $this->mainModel->get_Result_By_Column("SELECT GROUP_CONCAT(milk_dispatch_id) as dispatch_ids FROM `milk_dispatch` $where","dispatch_ids");  
        if($dispatch_ids){
          $dispatch_qty_sum = $this->mainModel->get_Result_By_Column("SELECT SUM(dis_total_qty) as tqty FROM `milk_dispatch_items` WHERE milk_dispatch_id IN($dispatch_ids) AND item_code='".$datas["iname"]."'","tqty"); 
        }   

        $issue_ids = $this->mainModel->get_Result_By_Column("SELECT GROUP_CONCAT(milk_issue_id) as issue_ids FROM `milk_issue` $where","issue_ids"); 
        if($issue_ids){
          $issue_qty_sum = $this->mainModel->get_Result_By_Column("SELECT SUM(qty) as tqty FROM `milk_issue_items` WHERE milk_issue_id IN($issue_ids) AND item_id='$item_id'","tqty"); 
        }    

        //Yesterday 
        $y_where = " WHERE rdate <= '$yesterday_date' ";
 
        $receipt_ids = $this->mainModel->get_Result_By_Column("SELECT GROUP_CONCAT(milk_receipt_id) as receipt_ids FROM `milk_receipt` $y_where","receipt_ids"); 
        if($receipt_ids){
          $yesterday_receipt_qty_sum = $this->mainModel->get_Result_By_Column("SELECT SUM(qty) as tqty FROM `milk_receipt_items` WHERE milk_receipt_id IN($receipt_ids) AND item_id='$item_id'","tqty"); 
        }  

        $dispatch_ids = $this->mainModel->get_Result_By_Column("SELECT GROUP_CONCAT(milk_dispatch_id) as dispatch_ids FROM `milk_dispatch` $y_where","dispatch_ids");  
        if($dispatch_ids){
          $yesterday_dispatch_qty_sum = $this->mainModel->get_Result_By_Column("SELECT SUM(dis_total_qty) as tqty FROM `milk_dispatch_items` WHERE milk_dispatch_id IN($dispatch_ids) AND item_code='".$datas["iname"]."'","tqty"); 
        }   

        $issue_ids = $this->mainModel->get_Result_By_Column("SELECT GROUP_CONCAT(milk_issue_id) as issue_ids FROM `milk_issue` $y_where","issue_ids"); 
        if($issue_ids){
          $yesterday_issue_qty_sum = $this->mainModel->get_Result_By_Column("SELECT SUM(qty) as tqty FROM `milk_issue_items` WHERE milk_issue_id IN($issue_ids) AND item_id='$item_id'","tqty"); 
        }  
        $receipt_qty_sum = (int) $receipt_qty_sum?$receipt_qty_sum:0;
        $dispatch_qty_sum = (int) $dispatch_qty_sum?$dispatch_qty_sum:0;
        $issue_qty_sum = (int) $issue_qty_sum?$issue_qty_sum:0;

        $ob = ((float) $yesterday_receipt_qty_sum-((float) $yesterday_dispatch_qty_sum+(float) $yesterday_issue_qty_sum));
        $cb = ((float)$ob + (float) $receipt_qty_sum)-((float) $dispatch_qty_sum+(float) $issue_qty_sum);
        $all_count = $receipt_qty_sum+$dispatch_qty_sum+$issue_qty_sum+$ob+$cb;
        if($all_count!=0):
        ?>
          
          <tr><td><?php echo $i+1; ?></td>
            <td><?php echo ucfirst($iname); ?></td> 
            <td class="text-right"><?php  echo $this->mainModel->oneDecimals($ob);
            //."--$yesterday_receipt_qty_sum---$yesterday_dispatch_qty_sum--$yesterday_issue_qty_sum"; ?></td> 
            <td class="text-right"><?php echo $this->mainModel->oneDecimals($receipt_qty_sum); ?></td> 
            <td class="text-right"><?php echo $this->mainModel->oneDecimals($dispatch_qty_sum); ?></td> 
            <td class="text-right"><?php echo $this->mainModel->oneDecimals($issue_qty_sum); ?></td> 
            <td class="text-right"><?php  echo $this->mainModel->oneDecimals($cb); ?></td> 
          </tr> 
        <?php $i++; endif;}
       
      ?>  
            </tbody>
            <!--  <tfoot>
              <tr class="qtyrow trsum"><th colspan="1"></th><th>Total</th><th><?php echo $tqty; ?></th></tr>
            </tfoot> -->
          </table>

           <?php } ?>
           
        </div>
       
    </div>  
</div>
     
 
<script type="text/javascript">
$(".changeData").on('change', function() {
  var current_val = $(this).val();
  var lineid = $(this).data("lineid");
  var type = $(this).data("type");
  var oldval =  $(this).data("oldval");
  var final_val = parseFloat(oldval) -  parseFloat(current_val);
  if(final_val>0){
    $("."+type+"_"+lineid).html(final_val);
    $("."+type+"_"+lineid).val(final_val);
    console.log(current_val,lineid);
  }else{
    alert("Please check value once.");
    //$("."+type+"_"+lineid).html("0");
    //$("."+type+"_"+lineid).val("0");
  }
  
});
</script>