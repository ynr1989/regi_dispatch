
<div class="row">
  <div class="col-xl-12 mx-auto">
    <!-- <h6 class="mb-0 text-uppercase">Horizontal Form</h6>
    <hr/> -->

    <span id="error"></span>

    <form >
    <div class="card border-top border-0 border-4 border-info">
      <div class="card-body">

        <div class="">
          <div class="card-title d-flex align-items-center">
            <div><i class="bx bxs-user me-1 font-22 text-info"></i>
            </div>
            <h4 class="mb-0 text-info">Products Dispatch Report</h4> 
          </div>
          <hr/>  

          <div class="col-md-6 mb-3">
            <label for="inputEnterYourName" class="col-sm-4 col-form-label">From Date</label>
            <div class="col-sm-8">
               <input class="result form-control" required="required" name="from_date"  value="<?php if(isset($_GET['from_date'])){ echo $_GET['from_date']; }else{echo date('d-m-Y');} ?>" type="text" id="date" placeholder="Date Picker...">
            </div>
          </div>

          <div class="col-md-6 mb-3">
            <label for="inputEnterYourName" class="col-sm-4 col-form-label">To Date</label>
            <div class="col-sm-8">
               <input class="result form-control" required="required" name="to_date"  value="<?php if(isset($_GET['to_date'])){ echo $_GET['to_date']; }else{echo date('d-m-Y');} ?>" type="text" id="to_date" placeholder="Date Picker...">
            </div>
          </div>
            
        <div class="col-md-6 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Route</label>
            <div class="col-sm-8"> 
 
              <?php
              $itemInfo = $this->mainModel->getRoutes('1');
              ?>
              <select name="route" class="multiple-select" data-placeholder="Choose anything" >
              <option value="">Select Route</option>
                <?php foreach($itemInfo as $itemInfos): ?>
                <option value="<?php echo $itemInfos['route']; ?>" <?php if(isset($_GET['route']) && $_GET['route'] == $itemInfos['route']){ echo "selected"; } ?>><?php echo $itemInfos['route']; ?></option>
                <?php endforeach; ?>
              </select>

            </div>
          </div> 
          
          <div class="col-md-6 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Vehicle Number</label>
            <div class="col-sm-8"> 

              <?php
              $itemInfo = $this->mainModel->getVehicles('1');
              ?>
              <select name="vehicle_no" class="multiple-select" data-placeholder="Choose anything" >
              <option value="">Select Vehicle</option>
                <?php foreach($itemInfo as $itemInfos): ?>
                <option value="<?php echo $itemInfos['vehicle_no']; ?>" <?php if(isset($_GET['vehicle_no']) && $_GET['vehicle_no'] == $itemInfos['vehicle_no'] && $_GET['vehicle_no']!=""){ echo "selected"; } ?>><?php echo $itemInfos['vehicle_no']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>    

          <div class="col-md-6 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Shift</label>
            <div class="col-sm-8">
              <select name="shift" class="form-control">
                <option value="">Select Shift</option>
                <option value="AM" <?php if(isset($_GET['shift']) && $_GET['shift'] == 'AM'){ echo "selected"; } ?>>AM</option>
                <option value="PM" <?php if(isset($_GET['shift']) && $_GET['shift'] == 'PM'){ echo "selected"; } ?>>PM</option>
              </select>
            </div>
          </div>

          <div class="col-md-6 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Item</label>
            <div class="col-sm-8">
             <?php
              $itemInfo = $this->mainModel->getItemsMaster("1");
              ?>
              <select name="item_code" class="multiple-select" data-placeholder="Choose anything" >
              <option value="">Select Item</option>
                <?php foreach($itemInfo as $itemInfos): ?>
                <option value="<?php echo $itemInfos['icod']; ?>" <?php if(isset($_GET['item_code']) && $_GET['item_code'] == $itemInfos['icod']){ echo "selected"; } ?>><?php echo $itemInfos['iname']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

          <div class="col-md-6 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Batch</label>
            <div class="col-sm-8">
             <?php
              $batchInfo = $this->mainModel->getProductBatchs();
              ?>
              <select name="batch_no" class="multiple-select" data-placeholder="Choose anything" >
              <option value="">Select Batch</option>
                <?php foreach($batchInfo as $batchInfos): ?>
                <option value="<?php echo $batchInfos['batch_no']; ?>" <?php if(isset($_GET['batch_no']) && $_GET['batch_no'] == $batchInfos['batch_no']){ echo "selected"; } ?>><?php echo $batchInfos['batch_no']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

           
          <div class="col-md-6 mb-3"> 
              <input type="submit" name="submit" class="btn btn-info mb-3" value="Get Details" /> 
              <a href="<?php echo base_url('milkdispatch'); ?>" class="btn btn-info mb-3">Reset</a>
          </div>
      </form>
   
     </div></div></div>
        
        <div class="table-repsonsive">
          
         <table id="example2" class="table table-striped table-bordered"> 
            <thead>

              <tr class="trsum"> 
                <th rowspan="2">S.No</th>  
                <th rowspan="2">Date</th>
                <th rowspan="2">Shift</th>
                <th rowspan="2">Route</th>
                <th rowspan="2">Vehicle</th>
                <th rowspan="2">DocInTime</th>
                <th rowspan="2">DocOutTime</th>
                <th rowspan="2">DocNo</th>
                <th rowspan="2">DocSecurity</th>
                <th rowspan="2">Product Name</th>

                <th colspan="6" class="text-center">Sales</th>
                <th colspan="6" class="text-center">Dispatch</th>
                <th colspan="2" class="text-center">Difference</th>
                <th rowspan="2">Batch Number</th>
                <th rowspan="2">Batch Qty</th>
                <th rowspan="2">Remarks</th>
              </tr>
              <tr class="trsum">
                 <th>Quantity</th>
                <th>Full Qty</th>
                <th>Lose Qty</th>
                <th>Full Tubs</th>
                <th>Lose Tubs</th>
                <th>Total Tubs</th>
               
                <th>Quantity</th> 
                <th>Full Qty</th>
                <th>Lose Qty</th>
                <th>Full Tubs</th>
                <th>Lose Tubs</th>
                <th>Total Tubs</th> 
                
                 <th>Total Tubs</th>
                <th>Total Qty</th>

              </tr>

              
            </thead>
            <tbody>
              <?php 
      if(isset($_GET['submit'])){

        $where = " WHERE t1.product_dispatch_id>0 ";  
        if(!isset($_GET['from_date']) && !isset($_GET['to_date'])){
          $tdate = date("Y-m-d");
          $where .=" AND t2.rdate ='$tdate'";
        } 

        if(((isset($_GET['from_date']) && $_GET['from_date']!="" && $_GET['to_date']=="") || ($_GET['from_date'] == $_GET['to_date'])) && ($_GET['from_date']!="")){        
          $where.= " AND t2.rdate='".$this->mainModel->dateFormatChange($_GET['from_date'],1)."'";
        }

        if(isset($_GET['to_date']) && $_GET['to_date']!="" && $_GET['from_date']==""){
          $where.= " AND t2.rdate='".$this->mainModel->dateFormatChange($_GET['to_date'],1)."'";
        }   

        if(isset($_GET['to_date']) && $_GET['to_date']!="" && $_GET['from_date']!="" && $_GET['from_date'] != $_GET['to_date']){          
          $fdate=$this->mainModel->dateFormatChange($_GET['from_date'],1); 
          $tdate = $this->mainModel->dateFormatChange($_GET['to_date'],1);
          $where.= "AND (t2.rdate between '$fdate' and '$tdate' )";
        }

        if(isset($_GET['shift']) && $_GET['shift']!=""){
          $shift = $_GET['shift'];
          $where.= " AND t2.shift='$shift'";
        } 
        
        if(isset($_GET['route']) && $_GET['route']!=""){
          $where.= " AND t2.route='".$_GET['route']."'";
        }
        
        if(isset($_GET['vehicle_no']) && $_GET['vehicle_no']!=""){
          $where.= " AND t2.vehicle_no='".$_GET['vehicle_no']."'";
        }

        if(isset($_GET['item_code']) && $_GET['item_code']!=""){
          $where.= " AND t1.item_code='".$_GET['item_code']."'";
        }
        if(isset($_GET['batch_no']) && $_GET['batch_no']!=""){
         $where.= " AND t1.batch_no='".$_GET['batch_no']."'";
        } 

        //$sql = "SELECT * FROM `milk_receipt` $where AND milk_receipt_id IN(SELECT milk_receipt_id FROM milk_receipt_items $where1 ) ORDER BY milk_receipt_id DESC"; 
        $sql = "SELECT * FROM products_dispatch_items t1 LEFT JOIN products_dispatch t2 ON t1.product_dispatch_id=t2.product_dispatch_id LEFT JOIN item_master t3 ON t1.item_code=t3.icod $where ORDER BY t1.product_dispatch_id DESC"; 
        $query = $this->db->query($sql);
        $row = $query->result_array();
 
         $i=0;   
        foreach($row as $datas){
          $sal_total_qty +=  number_format($datas['sal_total_qty'], 1, '.', '');
          $sal_full_qty += number_format($datas['sal_full_qty'], 1, '.', '');
          $sal_lose_qty += number_format($datas['sal_lose_qty'], 1, '.', ''); 
          $sal_full_tubs += number_format($datas['sal_full_tubs'], 1, '.', '');
          $sal_lose_tubs += number_format($datas['sal_lose_tubs'], 1, '.', '');
          $sal_total_tubs += number_format($datas['sal_total_tubs'], 1, '.', '');
          


          $disp_total_qty +=  number_format($datas['dis_total_qty'], 1, '.', '');
          $disp_full_qty += number_format($datas['dis_full_qty'], 1, '.', '');
          $disp_lose_qty += number_format($datas['dis_lose_qty'], 1, '.', ''); 
          $disp_full_tubs += number_format($datas['dis_full_tubs'], 1, '.', '');
          $disp_lose_tubs += number_format($datas['dis_lose_tubs'], 1, '.', '');
          $disp_total_tubs += number_format($datas['dis_total_tubs'], 1, '.', '');
          

          // 
          $diff_total_qty += $this->mainModel->numDifference($datas['sal_total_qty'],$datas['dis_total_qty']);
          $diff_total_tubs += $this->mainModel->numDifference($datas['sal_total_tubs'],$datas['dis_total_tubs']);  
          
           ?>
          
          <tr><td><?php echo $i+1; ?></td>
            <td><?php echo $datas['rdate']; ?></td> 
            <td><?php echo $datas['shift']; ?></td> 
            <td><?php echo $datas['route']; ?></td>
            <td><?php echo $datas['vehicle_no']; ?></td>
            <td><?php echo $datas['doc_in_time']; ?></td>
            <td><?php echo $datas['doc_out_time']; ?></td>
            <td><?php echo $datas['remarks']; ?></td>
            <td><?php echo $datas['login_id']; ?></td>
            <!--<td><?php echo $datas['item_code']; ?></td> -->
            <td><?php echo $datas['iname']; ?></td> 
            
            <td><?php echo $datas['sal_total_qty']; ?></td>
            <td><?php echo $datas['sal_full_qty']; ?></td>
            <td><?php echo $datas['sal_lose_qty']; ?></td>
            <td><?php echo $datas['sal_full_tubs']; ?></td>
            <td><?php echo $datas['sal_lose_tubs']; ?></td>
            <td><?php echo $datas['sal_total_tubs']; ?></td>
            
            <td><?php echo $datas['dis_total_qty']; ?></td>
            <td><?php echo $datas['sal_full_qty']; ?></td>
            <td><?php echo $datas['dis_lose_qty']; ?></td>
            <td><?php echo $datas['dis_full_tubs']; ?></td>
            <td><?php echo $datas['dis_lose_tubs']; ?></td>
            <td><?php echo $datas['dis_total_tubs']; ?></td>
            
             
           
            <td><?php echo $this->mainModel->numDifference($datas['sal_total_tubs'],$datas['dis_total_tubs']); ?></td>
             <td><?php echo $this->mainModel->numDifference($datas['sal_total_qty'],$datas['dis_total_qty']); ?></td>
            <td><?php echo $datas['batch_no']; ?></td> 
            <td>-</td> 
            <td><?php echo $datas['remarks']; ?></td>  
              
          </tr> 
        <?php $i++; }
      }   
      ?>  
            </tbody>

            <tfoot>
              <tr class="qtyrow trsum"><th>Total</th>
        <td colspan="9"></td> 
        <td><?php echo $sal_total_qty; ?></td>
        <td><?php echo $sal_full_qty; ?></td>
        <td><?php echo $sal_lose_qty; ?></td>
        <td><?php echo $sal_full_tubs; ?></td>
        <td><?php echo $sal_lose_tubs; ?></td>
        <td><?php echo $sal_total_tubs; ?></td> 
  
        <td class="t_dis_full_qty"><?php echo $disp_total_qty; ?></td>
        <td class="t_dis_full_qty"><?php echo $disp_full_qty; ?></td>
        <td class="t_dis_lose_qty"><?php echo $disp_lose_qty; ?></td>
        <td class="t_dis_full_tubs"><?php echo $disp_full_tubs; ?></td>
        <td class="t_dis_lose_tubs"><?php echo $disp_lose_tubs; ?></td>
        <td class="t_dis_total_tubs"><?php echo $disp_total_tubs; ?></td> 

        <td><?php echo $diff_total_qty; ?></td> 
        <td><?php echo $diff_total_tubs; ?></td>  
        <td colspan="3"></td>
        </tr>
            </tfoot>

          </table>

           
           
        </div>
       
    </div>  
</div>
     
 
<script type="text/javascript">
$(".changeData").on('change', function() {
  var current_val = $(this).val();
  var lineid = $(this).data("lineid");
  var type = $(this).data("type");
  var oldval =  $(this).data("oldval");
  var final_val = parseFloat(oldval) -  parseFloat(current_val);
  if(final_val>0){
    $("."+type+"_"+lineid).html(final_val);
    $("."+type+"_"+lineid).val(final_val);
    console.log(current_val,lineid);
  }else{
    alert("Please check value once.");
    //$("."+type+"_"+lineid).html("0");
    //$("."+type+"_"+lineid).val("0");
  }
  
});
</script>