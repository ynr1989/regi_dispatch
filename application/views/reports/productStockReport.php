
<div class="row">
  <div class="col-xl-12 mx-auto">
    <!-- <h6 class="mb-0 text-uppercase">Horizontal Form</h6>
    <hr/> -->

    <span id="error"></span>

    <form >
    <div class="card border-top border-0 border-4 border-info">
      <div class="card-body">

        <div class="">
          <div class="card-title d-flex align-items-center">
            <div><i class="bx bxs-user me-1 font-22 text-info"></i>
            </div>
            <h4 class="mb-0 text-info">Milk Products Issue Summary Report</h4> 
          </div>
          <hr/>  

          <div class="col-md-3 mb-3">
            <label for="inputEnterYourName" class="col-sm-4 col-form-label">From Date</label>
            <div class="col-sm-8">
               <input class="result form-control" required="required" name="from_date"  value="<?php if(isset($_GET['from_date'])){ echo $_GET['from_date']; }else{echo date('d-m-Y');} ?>" type="text" id="date" placeholder="Date Picker...">
            </div>
          </div>

          <div class="col-md-3 mb-3">
            <label for="inputEnterYourName" class="col-sm-4 col-form-label">To Date</label>
            <div class="col-sm-8">
               <input class="result form-control" required="required" name="to_date"  value="<?php if(isset($_GET['to_date'])){ echo $_GET['to_date']; }else{echo date('d-m-Y');} ?>" type="text" id="to_date" placeholder="Date Picker...">
            </div>
          </div> 

          <div class="col-md-3 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Route</label>
            <div class="col-sm-8">
             <?php
             $sql ="SELECT DISTINCT rname FROM routes_master WHERE status=1 AND sale_type='MILK' GROUP BY rname";  
              $rmaster = $this->mainModel->get_Result($sql);  
              ?>
              <select name="rname" class="multiple-select" data-placeholder="Choose anything" >
              <option value="">Select Route</option>
                <?php foreach($rmaster as $itemInfos): ?>
                <option value="<?php echo $itemInfos['rname']; ?>" <?php if(isset($_GET['rname']) && $_GET['rname'] == $itemInfos['rname']){ echo "selected"; } ?>><?php echo $itemInfos['rname']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div> 
           
          <div class="col-md-3 mb-3"> 
              <input type="submit" name="submit" class="btn btn-info mb-3" value="Get Details" /> 
              <a href="<?php echo base_url('product-stock-report'); ?>" class="btn btn-info mb-3">Reset</a>
          </div>
      </form>
   
     </div></div></div>
        
        <div class="table-repsonsive">
        <?php
        $sql = "SELECT * FROM item_master WHERE igroup = '101' AND status=1 ORDER BY misno";  
        $item_row = $this->mainModel->get_Result($sql); 
        
          ?>
         <table id="example3" class="table table-striped table-bordered"> 
            <thead>
              <tr class="trsum">
                <td colspan="2"></td>
                <?php
                foreach($item_row as $datas){
                $iname = strtoupper($datas["iname"]);

                ?>
                <td colspan="3" class="text-center"><strong><?php echo $iname; ?></strong></td>
              <?php } ?>
                <td colspan="3" class="text-center"><strong>Total</strong></td>
              </tr>
              </tr>
              <tr class="trsum"> 
                <th>S.No</th>
                <th>Route Name</th>
                <?php
                foreach($item_row as $datas){ ?>
                <th>Sale Qty</th>
                <th>Dispatch Qty</th>
                <th>Difference</th>
              <?php } ?>
                
                 <th>Sale Qty</th>
                <th>Dispatch Qty</th>
                <th>Difference</th>
              </tr> 
            </thead>
            <tbody>
      <?php 
      $date_query = 0;
      if(isset($_GET['submit'])){
        $yesterday_date = date( 'Y-m-d', strtotime( $_GET['from_date'] . ' -1 day' ) );
        $where = " WHERE status>0 ";  
        if(!isset($_GET['from_date']) && !isset($_GET['to_date']) && $_GET['from_date']=="" && $_GET['to_date']=""){
          $tdate = date("Y-m-d");
          $where .=" AND rdate ='$tdate'";
          $date_query = 1;
        } 

        if(((isset($_GET['from_date']) && $_GET['from_date']!="" && $_GET['to_date']=="") || ($_GET['from_date'] == $_GET['to_date'])) && ($_GET['from_date']!="")){       
          $where.= " AND rdate='".$this->mainModel->dateFormatChange($_GET['from_date'],1)."'";
          $date_query = 1;
        }

        if(isset($_GET['to_date']) && $_GET['to_date']!="" && $_GET['from_date']==""){
          $where.= " AND rdate='".$this->mainModel->dateFormatChange($_GET['to_date'],1)."'";
          $date_query = 1;
        }   
 
        if(isset($_GET['to_date']) && $_GET['to_date']!="" && $_GET['from_date']!="" && $_GET['from_date'] != $_GET['to_date']){ 
          $fdate=$this->mainModel->dateFormatChange($_GET['from_date'],1); 
          $tdate = $this->mainModel->dateFormatChange($_GET['to_date'],1);
          $where.= "AND (rdate between '$fdate' and '$tdate' )";
          $date_query = 1;
        }

        $sql ="SELECT DISTINCT rname FROM routes_master WHERE status=1 AND sale_type='MILK' GROUP BY rname";
        if(isset($_GET['rname']) && $_GET['rname']!=""){
          $mrname = $_GET['rname'];
          //$mrcodes = $this->mainModel->get_Result_By_Column("SELECT GROUP_CONCAT(rcode) as rcodes FROM routes_master WHERE status=1 AND sale_type='MILK' AND rname='".$rname."'", "rcodes");
          $sql ="SELECT DISTINCT rname FROM routes_master WHERE status=1 AND sale_type='MILK' AND rname='$mrname' GROUP BY rname";
        }
        
        //$sql ="SELECT DISTINCT rname FROM routes_master WHERE status=1 AND sale_type='MILK' GROUP BY rname";  
        $row = $this->mainModel->get_Result($sql); 
        $i=$tqty=$sqty_qty_sum=$dispatch_qty_sum=$issue_qty_sum=$yesterday_receipt_qty_sum=$yesterday_dispatch_qty_sum=$yesterday_issue_qty_sum=$sale_total=$diff_total=$dispatch_total=$sale_total1=$diff_total1=$dispatch_total1=0;
        foreach($row as $datas){ 
        $iname = strtoupper($datas["rname"]); 
        $rname = $datas["rname"]; 
        ?>
          
          <tr>
            <td><?php echo $i+1; ?></td>
            <td><?php echo ucfirst($iname); ?></td> 
            <?php
            $sale_total = $dispatch_total=$diff_total=0;
            foreach($item_row as $datas1){
            $iname = strtoupper($datas1["iname"]);
            $icode = $datas1["iname"]; 
            $rcodes = $this->mainModel->get_Result_By_Column("SELECT GROUP_CONCAT(rcode) as rcodes FROM routes_master WHERE status=1 AND sale_type='MILK' AND rname='".$rname."'", "rcodes");  
  //echo "<br>d==$rcodes<br>";
            $dispatch_ids = $this->mainModel->get_Result_By_Column("SELECT GROUP_CONCAT(milk_dispatch_id) as dispatch_ids FROM `milk_dispatch` $where  AND route IN(".$rcodes.")","dispatch_ids");

          //echo "<br>$dispatch_ids<br>";


            if($dispatch_ids){
              //$sqty_qty_sum = $this->mainModel->get_Result_By_Column("SELECT SUM(sal_total_qty) as sqty FROM `milk_dispatch_items` WHERE milk_dispatch_id IN($dispatch_ids) AND item_code='$icode'","sqty"); 
              $sqty_qty_sum = $this->mainModel->get_Result_By_Column("SELECT SUM(sal_total_qty) as sqty FROM `milk_dispatch_items` WHERE milk_dispatch_id IN($dispatch_ids) AND item_code='$icode'","sqty"); 
            }   

            if($dispatch_ids){
              $dispatch_qty_sum = $this->mainModel->get_Result_By_Column("SELECT SUM(dis_total_qty) as sqty FROM `milk_dispatch_items` WHERE milk_dispatch_id IN($dispatch_ids) AND item_code='$icode'","sqty");  
            } 

            
            $sqty_qty_sum = (float) $sqty_qty_sum?$sqty_qty_sum:0;
            $dispatch_qty_sum = (float) $dispatch_qty_sum?$dispatch_qty_sum:0;
            $diff_qty_sum = $sqty_qty_sum-$dispatch_qty_sum;
            $diff_qty_sum = (float) $diff_qty_sum?$diff_qty_sum:0;
            $total_sum = $sqty_qty_sum+$dispatch_qty_sum+$diff_qty_sum;
            $total_sum = (float) $total_sum?$total_sum:0;

            $sale_total += (float) $sqty_qty_sum;
            $diff_total += (float) $diff_qty_sum;
            $dispatch_total += (float) $dispatch_qty_sum;
            ?>
            <td><?php echo $sqty_qty_sum; ?> </td> 
            <td><?php echo $dispatch_qty_sum; ?></td> 
            <td><?php echo $diff_qty_sum; ?></td> 
          <?php } ?>
            
            <td><?php echo $sale_total; ?></td> 
            <td><?php echo $dispatch_total; ?></td> 
            <td><?php echo $diff_total; ?></td> 
          </tr> 
        <?php $i++; }
       
      ?>  
            </tbody>
             <tfoot align="right">
              <tr class="qtyrow trsum"><th colspan="1"></th><th>Total</th>
                <?php
                
                foreach($item_row as $datas2){
                $iname = strtoupper($datas2["iname"]);
                $icode = $datas2["iname"];
                $rcodes = $this->mainModel->get_Result_By_Column("SELECT GROUP_CONCAT(rcode) as rcodes FROM routes_master WHERE status=1 AND sale_type='MILK' AND rname='".$rname."'", "rcodes");  

                $dispatch_ids = $this->mainModel->get_Result_By_Column("SELECT GROUP_CONCAT(milk_dispatch_id) as dispatch_ids FROM `milk_dispatch` $where  AND route IN('".$rcodes."')","dispatch_ids");

                if($dispatch_ids){
                  $sqty_qty_sum = $this->mainModel->get_Result_By_Column("SELECT SUM(sal_total_qty) as sqty FROM `milk_dispatch_items` WHERE milk_dispatch_id IN($dispatch_ids) AND item_code='$icode'","sqty"); 
                }   

                if($dispatch_ids){
                  $dispatch_qty_sum = $this->mainModel->get_Result_By_Column("SELECT SUM(dis_total_qty) as sqty FROM `milk_dispatch_items` WHERE milk_dispatch_id IN($dispatch_ids) AND item_code='$icode'","sqty"); 
                }   

                
                $sqty_qty_sum = (float) $sqty_qty_sum?$sqty_qty_sum:0;
                $dispatch_qty_sum = (float) $dispatch_qty_sum?$dispatch_qty_sum:0;
                $diff_qty_sum = $sqty_qty_sum-$dispatch_qty_sum;
                $diff_qty_sum = (float) $diff_qty_sum?$diff_qty_sum:0;
                $total_sum = $sqty_qty_sum+$dispatch_qty_sum+$diff_qty_sum;
                $total_sum = (float) $total_sum?$total_sum:0;


                $sale_total1 += (float) $sqty_qty_sum;
                $dispatch_total1 += (float) $dispatch_qty_sum;
                $diff_total1 += (float) $diff_qty_sum;
                
                ?>
              <!-- <th><?php echo $sale_total; ?>---</th>
              <th><?php echo $dispatch_total1; ?></th>
              <th><?php echo $diff_total1; ?></th> -->

              <th></th>
              <th></th>
              <th></th>
              <?php } ?>
              <th></th>
              <th></th>
              <th></th>
              </tr>
            </tfoot> 
          </table>
<?php  } ?>
           
           
        </div>
       
    </div>  
</div>
     
 
<script type="text/javascript">
$(".changeData").on('change', function() {
  var current_val = $(this).val();
  var lineid = $(this).data("lineid");
  var type = $(this).data("type");
  var oldval =  $(this).data("oldval");
  var final_val = parseFloat(oldval) -  parseFloat(current_val);
  if(final_val>0){
    $("."+type+"_"+lineid).html(final_val);
    $("."+type+"_"+lineid).val(final_val);
    console.log(current_val,lineid);
  }else{
    alert("Please check value once.");
    //$("."+type+"_"+lineid).html("0");
    //$("."+type+"_"+lineid).val("0");
  }
  
});
</script>



<script>

//Page by page count
/*$(document).ready(function() {
    var table = $('#example3').DataTable({
      lengthChange: false,
      buttons: [ 'copy', 'excel', 'pdf', 'print'],


      "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(); 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            var columnscount = 34
            $( api.column( 0 ).footer() ).html('Total');
            $( api.column( 1 ).footer() ).html('Total');
            console.log("columnscount",columnscount);
            for(i=2;i<=columnscount;i++){
              total_i = api
                .column( i ,{ page: 'current'})
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                $( api.column( i ).footer() ).html(total_i); 
                 console.log("total_i",i,total_i);
            } 
          }, 
    }); 
    table.buttons().container()
      .appendTo( '#example2_wrapper .col-md-6:eq(0)' );
  }); */
 

//Complete count in single row
 $(document).ready(function() {
    var table = $('#example3').DataTable({
      lengthChange: false,
      buttons: [ 'copy', 'excel', 'pdf', 'print'],


      "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(); 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            var columnscount = 34
            $( api.column( 0 ).footer() ).html('Total');
            $( api.column( 1 ).footer() ).html('Total');
            console.log("columnscount",columnscount);
            for(i=2;i<=columnscount;i++){
              total_i = api
                .column( i )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                $( api.column( i ).footer() ).html(total_i);  
                 console.log("total_i",i,total_i);
            } 
          }, 
    }); 
    table.buttons().container()
      .appendTo( '#example3_wrapper .col-md-6:eq(0)' );
  }); 
</script>