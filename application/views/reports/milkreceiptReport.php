
<div class="row">
  <div class="col-xl-12 mx-auto">
    <!-- <h6 class="mb-0 text-uppercase">Horizontal Form</h6>
    <hr/> -->

    <span id="error"></span>

    <form >
    <div class="card border-top border-0 border-4 border-info">
      <div class="card-body">

        <div class="">
          <div class="card-title d-flex align-items-center">
            <div><i class="bx bxs-user me-1 font-22 text-info"></i>
            </div>
            <h4 class="mb-0 text-info">Milk Receipt Report</h4> 
          </div>
          <hr/>  

          <div class="col-md-6 mb-3">
            <label for="inputEnterYourName" class="col-sm-4 col-form-label">From Date</label>
            <div class="col-sm-8">
               <input class="result form-control" required="required" name="from_date"  value="<?php if(isset($_GET['from_date'])){ echo $_GET['from_date']; }else{echo date('d-m-Y');} ?>" type="text" id="date" placeholder="Date Picker...">
            </div>
          </div>

          <div class="col-md-6 mb-3">
            <label for="inputEnterYourName" class="col-sm-4 col-form-label">To Date</label>
            <div class="col-sm-8">
               <input class="result form-control" required="required" name="to_date"  value="<?php if(isset($_GET['to_date'])){ echo $_GET['to_date']; }else{echo date('d-m-Y');} ?>" type="text" id="to_date" placeholder="Date Picker...">
            </div>
          </div>

          <div class="col-md-6 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Shift</label>
            <div class="col-sm-8">
              <select name="shift" class="form-control">
                <option value="">Select Shift</option>
                <option value="AM" <?php if(isset($_GET['shift']) && $_GET['shift'] == 'AM'){ echo "selected"; } ?>>AM</option>
                <option value="PM" <?php if(isset($_GET['shift']) && $_GET['shift'] == 'PM'){ echo "selected"; } ?>>PM</option>
              </select>
            </div>
          </div>

          <div class="col-md-6 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Item</label>
            <div class="col-sm-8">
             <?php
              $itemInfo = $this->mainModel->getItemsMaster();
              ?>
              <select name="item_code" class="multiple-select" data-placeholder="Choose anything" >
              <option value="">Select Item</option>
                <?php foreach($itemInfo as $itemInfos): ?>
                <option value="<?php echo $itemInfos['icod']; ?>" <?php if(isset($_GET['item_code']) && $_GET['item_code'] == $itemInfos['icod']){ echo "selected"; } ?>><?php echo $itemInfos['iname']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

          <div class="col-md-6 mb-3">
            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Batch</label>
            <div class="col-sm-8">
             <?php
              $batchInfo = $this->mainModel->getBatchs();
              ?>
              <select name="batch_no" class="multiple-select" data-placeholder="Choose anything" >
              <option value="">Select Batch</option>
                <?php foreach($batchInfo as $batchInfos): ?>
                <option value="<?php echo $batchInfos['batch_no']; ?>" <?php if(isset($_GET['batch_no']) && $_GET['batch_no'] == $batchInfos['batch_no']){ echo "selected"; } ?>><?php echo $batchInfos['batch_no']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

           
          <div class="col-md-6 mb-3"> 
              <input type="submit" name="submit" class="btn btn-info mb-3" value="Get Details" /> 
              <a href="<?php echo base_url('milkdispatch'); ?>" class="btn btn-info mb-3">Reset</a>
          </div>
      </form>
   
     </div></div></div>
        
        <div class="table-repsonsive">
          
         <table id="example2" class="table table-striped table-bordered"> 
            <thead>
              <tr class="trsum"> 
                <th>S.No</th>
                <th>Date</th>
                <th>Shift</th>
                <th>Product Name</th>
                <th>Qty</th>
                <th>Batch No</th>
                <th>Remarks</th> 
              </tr> 
            </thead>
            <tbody>
              <?php 
      if(isset($_GET['submit'])){

        $where = " WHERE t1.milk_receipt_id>0 ";  
        if(!isset($_GET['from_date']) && !isset($_GET['to_date'])){
          $tdate = date("Y-m-d");
          $where .=" AND t2.rdate ='$tdate'";
        } 

        if(((isset($_GET['from_date']) && $_GET['from_date']!="" && $_GET['to_date']=="") || ($_GET['from_date'] == $_GET['to_date'])) && ($_GET['from_date']!="")){        
          $where.= " AND t2.rdate='".$this->mainModel->dateFormatChange($_GET['from_date'],1)."'";
        }

        if(isset($_GET['to_date']) && $_GET['to_date']!="" && $_GET['from_date']==""){
          $where.= " AND t2.rdate='".$this->mainModel->dateFormatChange($_GET['to_date'],1)."'";
        }   

        if(isset($_GET['to_date']) && $_GET['to_date']!="" && $_GET['from_date']!="" && $_GET['from_date'] != $_GET['to_date']){          
          $fdate=$this->mainModel->dateFormatChange($_GET['from_date'],1); 
          $tdate = $this->mainModel->dateFormatChange($_GET['to_date'],1);
          $where.= "AND (t2.rdate between '$fdate' and '$tdate' )";
        }

        if(isset($_GET['shift']) && $_GET['shift']!=""){
          $shift = $_GET['shift'];
          $where.= " AND t2.shift='$shift'";
        } 

        if(isset($_GET['item_code']) && $_GET['item_code']!=""){
          $where.= " AND t1.item_code='".$_GET['item_code']."'";
        }
        if(isset($_GET['batch_no']) && $_GET['batch_no']!=""){
         $where.= " AND t1.batch_no='".$_GET['batch_no']."'";
        } 

        //$sql = "SELECT * FROM `milk_receipt` $where AND milk_receipt_id IN(SELECT milk_receipt_id FROM milk_receipt_items $where1 ) ORDER BY milk_receipt_id DESC"; 
        $sql = "SELECT * FROM milk_receipt_items t1 LEFT JOIN milk_receipt t2 ON t1.milk_receipt_id=t2.milk_receipt_id $where ORDER BY t1.milk_receipt_id DESC"; 
        $query = $this->db->query($sql);
        $row = $query->result_array();
 
         $i= $tqty = 0;
        foreach($row as $datas){ $tqty +=$datas['qty']; ?>
          
          <tr><td><?php echo $i+1; ?></td>
            <td><?php echo $this->mainModel->dateFormatChange($datas['rdate']); ?></td> 
            <td><?php echo $datas['shift']; ?></td> 
            <td><?php echo $datas['item_code']; ?></td> 
            <td><?php echo $datas['qty']; ?></td> 
            <td><?php echo $datas['batch_no']; ?></td> 
            <td><?php echo $datas['remarks']; ?></td>  
              
          </tr> 
        <?php $i++; }
      }   
      ?>  
            </tbody>
             <tfoot>
              <tr class="qtyrow trsum"><th colspan="3"></th><th>Total</th><th><?php echo $tqty; ?></th><th colspan="2"></th></tr>
            </tfoot>
          </table>

           
           
        </div>
       
    </div>  
</div>
     
 
<script type="text/javascript">
$(".changeData").on('change', function() {
  var current_val = $(this).val();
  var lineid = $(this).data("lineid");
  var type = $(this).data("type");
  var oldval =  $(this).data("oldval");
  var final_val = parseFloat(oldval) -  parseFloat(current_val);
  if(final_val>0){
    $("."+type+"_"+lineid).html(final_val);
    $("."+type+"_"+lineid).val(final_val);
    console.log(current_val,lineid);
  }else{
    alert("Please check value once.");
    //$("."+type+"_"+lineid).html("0");
    //$("."+type+"_"+lineid).val("0");
  }
  
});
</script>