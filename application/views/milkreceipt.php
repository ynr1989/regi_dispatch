
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  

<div class="row">
	<div class="col-xl-12 mx-auto">
		<!-- <h6 class="mb-0 text-uppercase">Horizontal Form</h6>
		<hr/> -->

		<span id="error"></span>

		<form method="post" id="insert_form">
		<div class="card border-top border-0 border-4 border-info">
			<div class="card-body">
				<div class="border p-4 rounded">
					<div class="card-title d-flex align-items-center">
						<div><i class="bx bxs-user me-1 font-22 text-info"></i>
						</div>
						<h4 class="mb-0 text-info">Milk Receipt</h4>
					</div>
					<hr/> 

					<div class="row mb-3">
						<label for="inputEnterYourName" class="col-sm-3 col-form-label">Date</label>
						<div class="col-sm-9">
							<input class="result form-control" name="rdate" value="<?php echo date('d-m-Y'); ?>" type="text" id="date" placeholder="Date Picker...">
						</div>
					</div>
					<div class="row mb-3">
						<label for="inputPhoneNo2" class="col-sm-3 col-form-label">Shift</label>
						<div class="col-sm-9">
							<select name="shift" class="form-control" required="required">
								<option value="">Select Shift</option>
								<option value="AM">AM</option>
								<option value="PM">PM</option>
							</select>
						</div>
					</div>
					 
        <div class="table-repsonsive">
          
          <table class="table table-bordered" id="item_table">
            <thead>
              <tr class="trsum">
              	<th>Product Name</th>
                <th>Product Code</th> 
                <th>Batch No</th>
                <th>Qty</th>
                <th>Remarks</th>
                <th><button type="button" name="add" class="btn btn-success btn-xs add1"><span class="glyphicon glyphicon-plus"></span></button></th>
              </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
              <tr class="qtyrow trsum"><th colspan="2"></th><th>Total</th><th><span class="totalqty"></span></th><th colspan="2"></th></tr>
            </tfoot>
          </table>

          <div class="row mb-3">
						<label for="inputEnterYourName" class="col-sm-3 col-form-label">Remarks</label>
						<div class="col-sm-9">
							<textarea name="bremarks" class="form-control" id="bremarks" placeholder="Enter Remarks"></textarea> 
						</div>
					</div>

          <div align="center">
            <input type="submit" name="submit" class="btn btn-info sbutton" value="Submit" />
            <!--<input type="submit" name="submit" class="btn btn-warning" value="Submit" />-->
          </div>
        </div>
      </form>
    </div> 

 
				</div>
			</div>
		</div>
	</div>
</div>


<script> 

	//$(".add1").trigger("click");
    $(document).ready(function(){
      
      var count = 0;

      $(document).on('click', '.add1', function(){
        count++;
        var html = '';
        html += '<tr>';
        html += '<td><select name="item_id[]" class="form-control item_master mdb-select" data-line_id="'+count+'"><option value="">Select Product</option><?php echo $this->mainModel->fill_select_box("0"); ?></select></td>';
        html += '<td><input type="text" name="item_code[]" class="form-control item_code" id="item_code'+count+'" readonly/></td>';
        //html += '<td><input type="text" name="item_name[]" class="form-control item_name" id="item_name'+count+'"/></td>';
        html += '<td><input type="text" name="batch_no[]" class="form-control batch_no" id="batch_no'+count+'"/></td>';
        html += '<td><input type="text" name="qty[]" class="form-control qty sum qt'+count+'" id="qty'+count+'"/></td>';
         html += '<td><input type="text" name="remarks[]" class="form-control qty" id="remarks'+count+'"/></td>';
        //html += '<td><select name="item_sub_category[]" class="form-control item_sub_category" id="item_sub_category'+count+'"><option value="">Select Sub Category</option></select></td>';
        html += '<td><button type="button" name="remove" class="btn btn-danger btn-xs remove"><span class="glyphicon glyphicon-minus"></span></button></td>';
        $('tbody').append(html);
      });

      $(document).on('click', '.remove', function(){
        $(this).closest('tr').remove();
      });


      $(document).on('change', '.item_master', function(){
        var item_id = $(this).val();
        var line_id = $(this).data('line_id');
        var item_code = $(this).find(':selected').data('icode');
        //var iname = $(this).find(':selected').data('iname');
        //$('#item_name'+line_id).val(iname);
        $('#item_code'+line_id).val(item_code);
        //console.log(item_id+"=="+line_id+"=="+item_code+"=="+iname);
        $(".add1").trigger("click");	
      });

      /*$(document).on('change', '.item_category', function(){
        var category_id = $(this).val();
        var sub_category_id = $(this).data('sub_category_id');
        $.ajax({
          url:"fill_sub_category.php",
          method:"POST",
          data:{category_id:category_id},
          success:function(data)
          {
            var html = '<option value="">Select Sub Category</option>';
            html += data;
            $('#item_sub_category'+sub_category_id).html(html);
          }
        })
      });*/

      $('#insert_form').on('submit', function(event){
        event.preventDefault();
        var error = '';
        $('.item_name').each(function(){
          var count = 1;
          if($(this).val() == '')
          {
            error += '<p>Enter Item name at '+count+' Row</p>';
            return false;
          }
          count = count + 1;
        });

        $('.item_category').each(function(){
          var count = 1;

          if($(this).val() == '')
          {
            error += '<p>Select Item Category at '+count+' row</p>';
            return false;
          }

          count = count + 1;

        });

        $('.item_sub_category').each(function(){

          var count = 1;

          if($(this).val() == '')
          {
            error += '<p>Select Item Sub category '+count+' Row</p> ';
            return false;
          } 
          count = count + 1; 
        });

        /*$('.qty').each(function(){
          var count = 1;
          if($(this).val() == '')
          {
            error += '<p>Enter Qty '+count+' Row</p> ';
            return false;
          }
          count = count + 1;
        });*/

        var form_data = $(this).serialize();

        if(error == '')
        {
          $(".sbutton").attr('disabled', 'disabled');
          $.ajax({
            url:"<?php echo base_url('saveMilkreceipt'); ?>",
            method:"POST",
            data:form_data,
            success:function(data)
            {
              console.log("data",data)
              if(data == 'ok')
              {
                $('#item_table').find('tr:gt(0)').remove();
                $('#error').html('<div class="alert alert-success">Data Saved Successfully...</div>');
                $('#insert_form').trigger("reset");
                $(".sbutton").removeAttr('disabled');
              }else{
                $('#error').html('<div class="alert alert-danger">'+data+'</div>'); 
                $(".sbutton").removeAttr('disabled');
              }
            }
          });
        }
        else
        {
          $('#error').html('<div class="alert alert-danger">'+error+'</div>');
        }

      }); 
    });




$(document).on("change", ".sum", function() {
  var sum1 = 0; 
  $(".sum").each(function(){
      sum1 += +$(this).val(); 
  }); 
  $(".totalqty").html(sum1);
});
</script>
