
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  

<div class="row">
	<div class="col-xl-12 mx-auto">
		<!-- <h6 class="mb-0 text-uppercase">Horizontal Form</h6>
		<hr/> -->

		<span id="error"></span>

		<form >
		<div class="card border-top border-0 border-4 border-info">
			<div class="card-body">
				<div class="border p-4 rounded">
					<div class="card-title d-flex align-items-center">
						<div><i class="bx bxs-user me-1 font-22 text-info"></i>
						</div>
						<h4 class="mb-0 text-info">Milk Products Dispatch</h4>	
					</div>
					<hr/>  
					<div class="col-md-6 mb-3">
						<label for="inputEnterYourName" class="col-sm-4 col-form-label">Date</label>
						<div class="col-sm-8">
							 <input class="result form-control" required="required" name="rdate"  value="<?php if(isset($_GET['rdate'])){ echo $_GET['rdate']; }else{echo date('d-m-Y');} ?>" type="text" id="date" placeholder="Date Picker...">
						</div>
					</div>

					<div class="col-md-6 mb-3">
						<label for="inputPhoneNo2" class="col-sm-4 col-form-label">Vehicle Number</label>
						<div class="col-sm-8">
							<input type="text" value="<?php if(isset($_GET['vehicle_no'])){ echo $_GET['vehicle_no']; } ?>" name="vehicle_no" required="required" class="form-control" id="vehicle_no">
						</div>
					</div>

			          <div class="col-md-6 mb-3">
			            <label for="inputPhoneNo2" class="col-sm-4 col-form-label">Shift</label>
			            <div class="col-sm-8">
			              <select name="shift" class="form-control" required="required">
			                <option value="">Select Shift</option>
			                <option value="AM" <?php if(isset($_GET['shift']) && $_GET['shift'] == 'AM'){ echo "selected"; } ?>>AM</option>
			                <option value="PM" <?php if(isset($_GET['shift']) && $_GET['shift'] == 'PM'){ echo "selected"; } ?>>PM</option>
			              </select>
			            </div>
			          </div>

			          <div class="col-md-6 mb-3">
						<label for="inputEnterYourName" class="col-sm-4 col-form-label">Doc in time</label>
						<div class="col-sm-8">
							 <input value="<?php if(isset($_GET['doc_in_time'])){ echo $_GET['doc_in_time']; }else{echo date('h:i');} ?>" class="result form-control" required="required" name="doc_in_time" type="time" id="" placeholder="Date Picker...">
						</div>
					</div>

					<div class="col-md-6 mb-3">
						<label for="inputPhoneNo2" class="col-sm-4 col-form-label">Route</label>
						<div class="col-sm-8">
							<input value="<?php if(isset($_GET['route'])){ echo $_GET['route']; } ?>" type="text" name="route" required="required" class="form-control" id="route">
						</div>
					</div>

					<div class="col-md-6 mb-3">
						<label for="inputEnterYourName" class="col-sm-4 col-form-label">Doc out time</label>
						<div class="col-sm-8">
							 <input value="<?php if(isset($_GET['doc_out_time'])){ echo $_GET['doc_out_time']; }else{echo date('h:i');} ?>" class="result form-control" required="required" name="doc_out_time"  type="time" id="" placeholder="Date Picker...">
						</div>
					</div>
					
					<div class="col-md-6 mb-3">
						<label for="inputPhoneNo2" class="col-sm-4 col-form-label">Doc Number</label>
						<div class="col-sm-8">
							<input type="text" value="<?php if(isset($_GET['doc_no'])){ echo $_GET['doc_no']; } ?>" name="doc_no" required="required" class="form-control" id="doc_no">
						</div>
					</div>
					
					<div class="col-md-6 mb-3">
						<label for="inputPhoneNo2" class="col-sm-4 col-form-label">Doc Guard</label>
						<div class="col-sm-8">
							<input type="text" value="<?php if(isset($_GET['docsec_name'])){ echo $_GET['docsec_name']; } ?>" name="docsec_name" required="required" class="form-control" id="docsec_name">
						</div>
					</div>
					
					<div class="col-md-6 mb-3"> 
			            <input type="submit" name="submit" class="btn btn-info mb-3" value="Get Details" /> 
			            <a href="<?php echo base_url('milkdispatch'); ?>" class="btn btn-info mb-3">Reset</a>
		          </div>
      </form>
	 	

     
	 	<?php if($this->session->flashdata('message')!=''): ?>
	 		<div class="smessage">
	 			<div class="col-md-12 mb-3"> 
        <div class="alert alert-success">
           <?php echo $this->session->flashdata('message'); ?>
         </div>
       </div>
     </div>
      <?php  endif; 

      if($this->session->flashdata('message1')!=''): ?>
      	<div class="smessage">
      		<div class="col-md-10 mb-3"> 
         <div class="alert alert-danger">
      		 	<?php echo $this->session->flashdata('message1'); ?>
      	</div>
      </div>
      </div>
      	<?php
      endif; ?>

       <?php
      if(isset($_GET['submit'])){

		$mddata = array(
				"rdate" => $_GET['rdate'],
				"shift" => $_GET['shift'],
				"route" => $_GET['route'],
				"vehicle_no" => $_GET['vehicle_no']
		); 
		$checkCount = $this->mainModel->checkProductDispatchRecordCount($mddata);
		if(count($checkCount)>0){ ?>
		<!-- 	<div class="smessage">
      		<div class="col-md-10 mb-3"> 
         <div class="alert alert-danger">
      		 	Data already exist. If you want to continue ok...
      	</div>
      </div>
      </div> -->
		<?php }
	}
		?>


     <form method="post" id="insert_form" action="<?php echo base_url('saveProductdispatch'); ?>">
				<input type="hidden" name="mddata[rdate]" value="<?php if(isset($_GET['rdate'])){ echo $_GET['rdate']; }else{echo date('d-m-Y');} ?>">
				<input type="hidden" name="mddata[vehicle_no]" value="<?php if(isset($_GET['vehicle_no'])){ echo $_GET['vehicle_no']; } ?>">
				<input type="hidden" name="mddata[shift]" value="<?php if(isset($_GET['shift'])){ echo $_GET['shift']; } ?>">
				<input type="hidden" name="mddata[route]" value="<?php if(isset($_GET['route'])){ echo $_GET['route']; } ?>">
				<input type="hidden" name="mddata[doc_in_time]" value="<?php if(isset($_GET['doc_in_time'])){ echo $_GET['doc_in_time']; }else{echo date('h:i');} ?>">
				<input type="hidden" name="mddata[doc_out_time]" value="<?php if(isset($_GET['doc_out_time'])){ echo $_GET['doc_out_time']; }else{echo date('h:i');} ?>">
        <div class="table-repsonsive">
          
          <table class="table table-bordered table-repsonsive" id="item_table">
            <thead>
            	<tr class="trsum"> <th rowspan="2">S.No</th>
                <th rowspan="2">Product Name</th>  
            		<th colspan="6" class="text-center">Sales</th>
            		<th colspan="6" class="text-center">Dispatch</th>
            		<th rowspan="2">Batch Number</th>
            	</tr>
              <tr class="trsum">
              	 <th>Quantity</th>
                <th>Full Qty</th>
                <th>Lose Qty</th>
                <th>Full Tubs</th>
                <th>Lose Tubs</th>
                <th>Total Tubs</th>
               
 								<th>Quantity</th> 
                <th>Full Qty</th>
                <th>Lose Qty</th>
                <th>Full Tubs</th>
                <th>Lose Tubs</th>
                <th>Total Tubs</th> 
                
              </tr>
            </thead>
            <tbody>
            	<?php 
            	$sal_full_qty=$sal_qty = 0;
				$sal_lose_qty = 0;
				$sal_full_tubs = 0;
				$sal_lose_tubs = 0;
				$sal_total_tubs = 0; 
				$sal_total_qty = 0;
				$sal_sachat_qty = 0;

				
				$disp_full_qty = 0;
				$disp_lose_qty = 0;
				$disp_full_tubs = 0;
				$disp_lose_tubs = 0;
				$disp_total_tubs = 0; 
				$disp_total_qty = 0;
				$disp_qty=0;
			if(isset($_GET['submit'])){
				$data = $this->mainModel->get_product_results($_GET['rdate'],$_GET['shift'],$_GET['route']);

					
				  
				$ProductsLoading = $data->ProductsLoading;

				 
				

				for($i=0;$i<count($ProductsLoading);$i++){
					
					$sal_qty += number_format($ProductsLoading[$i]->Quantity, 1, '.', '');
					$sal_full_qty += number_format($ProductsLoading[$i]->FullQty, 1, '.', '');
					$sal_lose_qty += number_format($ProductsLoading[$i]->LoseQty, 1, '.', ''); 
					$sal_full_tubs += number_format($ProductsLoading[$i]->FullTubs, 1, '.', '');
					$sal_lose_tubs += number_format($ProductsLoading[$i]->LoseTubs, 1, '.', '');
					$sal_total_tubs += number_format($ProductsLoading[$i]->TotalTubs, 1, '.', ''); 
					$sal_total_qty +=  number_format($ProductsLoading[$i]->TotalQty, 1, '.', ''); 


					$disp_qty += number_format($ProductsLoading[$i]->Quantity, 1, '.', '');
					$disp_full_qty += number_format($ProductsLoading[$i]->FullQty, 1, '.', '');
					$disp_lose_qty += number_format($ProductsLoading[$i]->LoseQty, 1, '.', ''); 
					$disp_full_tubs += number_format($ProductsLoading[$i]->FullTubs, 1, '.', '');
					$disp_lose_tubs += number_format($ProductsLoading[$i]->LoseTubs, 1, '.', '');
					$disp_total_tubs += number_format($ProductsLoading[$i]->TotalTubs, 1, '.', '');

					$disp_total_qty +=  number_format($ProductsLoading[$i]->TotalQty, 1, '.', '');
					 ?>
					<input type="hidden" name="ditems[<?php echo $i; ?>][line_no]" value="<?php echo $i; ?>">
					<input type="hidden" name="ditems[<?php echo $i; ?>][item_code]" value="<?php echo $ProductsLoading[$i]->ProductCode; ?>">
				 <input type="hidden" name="ditems[<?php echo $i; ?>][sal_full_qty]" value="<?php echo number_format($ProductsLoading[$i]->FullQty, 1, '.', ''); ?>">
					<input type="hidden" name="ditems[<?php echo $i; ?>][sal_lose_qty]" value="<?php echo number_format($ProductsLoading[$i]->LoseQty, 1, '.', ''); ?>">
					<input type="hidden" name="ditems[<?php echo $i; ?>][sal_full_tubs]" value="<?php echo number_format($ProductsLoading[$i]->FullTubs, 1, '.', ''); ?>">
					<input type="hidden" name="ditems[<?php echo $i; ?>][sal_lose_tubs]" value="<?php echo number_format($ProductsLoading[$i]->LoseTubs, 1, '.', ''); ?>">
					<input type="hidden" name="ditems[<?php echo $i; ?>][sal_total_tubs]" value="<?php echo number_format($ProductsLoading[$i]->TotalTubs, 1, '.', ''); ?>">
					 <input type="hidden" name="ditems[<?php echo $i; ?>][sal_total_qty]" value="<?php echo number_format($ProductsLoading[$i]->Quantity, 1, '.', ''); ?>">



					<tr><td><?php echo $i+1; ?></td>
						<td><?php echo $ProductsLoading[$i]->ProductName; ?></td>
						<td><?php echo number_format($ProductsLoading[$i]->Quantity, 1, '.', ''); ?></td>
						 <td><?php echo number_format($ProductsLoading[$i]->FullQty, 1, '.', ''); ?></td>
						<td><?php echo number_format($ProductsLoading[$i]->LoseQty, 1, '.', ''); ?></td>
						<td><?php echo number_format($ProductsLoading[$i]->FullTubs, 0, '.', ''); ?></td>
						<td><?php echo number_format($ProductsLoading[$i]->LoseTubs, 0, '.', ''); ?></td>
						<td><?php echo number_format($ProductsLoading[$i]->TotalTubs, 0, '.', ''); ?></td>
						 

						 
						 <td><?php echo number_format($ProductsLoading[$i]->Quantity, 1, '.', ''); ?></td>
						<td><?php echo number_format($ProductsLoading[$i]->FullQty, 1, '.', ''); ?></td>
						<td><?php echo number_format($ProductsLoading[$i]->LoseQty, 1, '.', ''); ?></td>
						<td><?php echo number_format($ProductsLoading[$i]->FullTubs, 0, '.', ''); ?></td>
						<td><?php echo number_format($ProductsLoading[$i]->LoseTubs, 0, '.', ''); ?></td>
						<td><?php echo number_format($ProductsLoading[$i]->TotalTubs, 0, '.', ''); ?></td>

						<input type="hidden" name="ditems[<?php echo $i; ?>][dis_full_qty]" value="<?php echo number_format($ProductsLoading[$i]->FullQty, 1, '.', ''); ?>">
					<input type="hidden" name="ditems[<?php echo $i; ?>][dis_lose_qty]" value="<?php echo number_format($ProductsLoading[$i]->LoseQty, 1, '.', ''); ?>">
					<input type="hidden" name="ditems[<?php echo $i; ?>][dis_full_tubs]" value="<?php echo number_format($ProductsLoading[$i]->FullTubs, 1, '.', ''); ?>">
					<input type="hidden" name="ditems[<?php echo $i; ?>][dis_lose_tubs]" value="<?php echo number_format($ProductsLoading[$i]->LoseTubs, 1, '.', ''); ?>">
					 
					<input type="hidden" name="ditems[<?php echo $i; ?>][dis_total_tubs]" value="<?php echo number_format($ProductsLoading[$i]->TotalTubs, 1, '.', ''); ?>">

						 
						<td>
							<?php
							$batchInfo = $this->mainModel->getProductBatchs($ProductsLoading[$i]->ProductCode);
							?>
							<select name="batch_no[<?php echo $i; ?>][]" class="multiple-select" data-placeholder="Choose anything" multiple="multiple">
              <!--   <option value="">Select Batch</option> -->
                <?php foreach($batchInfo as $batchInfos): ?>
                <option value="<?php echo $batchInfos['batch_no']; ?>"><?php echo $batchInfos['batch_no']; ?></option>
                <?php endforeach; ?>
              </select>

						</td>
					</tr> 
					 
					<input type="hidden" name="ditems[<?php echo $i; ?>][dis_total_qty]" value="<?php echo number_format($ProductsLoading[$i]->Quantity, 1, '.', ''); ?>">

				<?php }
			}		
			?>	
            </tbody>
            <tfoot>
            	<tr class="trsum">
        <th>Total</th>
        <td></td> 
        <td><?php echo $sal_qty; ?></td>
        <td><?php echo $sal_full_qty; ?></td>
        <td><?php echo $sal_lose_qty; ?></td>
        <td><?php echo $sal_full_tubs; ?></td>
        <td><?php echo $sal_lose_tubs; ?></td>
        <td><?php echo $sal_total_tubs; ?></td>  

 				<td class="t_dis_qty"><?php echo $disp_qty; ?></td>
        <td class="t_dis_full_qty"><?php echo $disp_full_qty; ?></td>
        <td class="t_dis_lose_qty"><?php echo $disp_lose_qty; ?></td>
        <td class="t_dis_full_tubs"><?php echo $disp_full_tubs; ?></td>
        <td class="t_dis_lose_tubs"><?php echo $disp_lose_tubs; ?></td>
        <td class="t_dis_total_tubs"><?php echo $disp_total_tubs; ?></td> 
        <td> </td> 
      </tr>
            </tfoot>
          </table>

          <div class="row mb-3">
						<label for="inputEnterYourName" class="col-sm-3 col-form-label">Remarks</label>
						<div class="col-sm-9">
							<textarea name="remarks" class="form-control" id="bremarks" placeholder="Enter Remarks"></textarea> 
						</div>
					</div>

          <div align="center">
            <input type="submit" name="submit" class="btn btn-info" value="Submit" />
            <!--<input type="submit" name="submit" class="btn btn-warning" value="Submit" />-->
          </div>
        </div>
      </form>
    </div> 

 
				</div>
			</div>
		</div>
	</div>
</div>

 
