<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/ 
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['loginAction'] = 'Home/loginAction';
$route['change-password'] = "Dashboard/changePassword";
$route['submitChangePassword'] = "Dashboard/submitChangePassword";
$route['profileSubmit'] = "Dashboard/profileSubmit";
$route['update-profile'] = "Dashboard/updateProfile";
$route['logout'] = 'Dashboard/logout';

$route['form1'] = 'Dashboard/form1';
$route['tabel'] = 'Dashboard/tabel';
$route['dashboard'] = 'Dashboard/index';


$route['milkreceipt'] = 'Dashboard/milkreceipt';
$route['milkissue'] = 'Dashboard/milkissue'; //S2
$route['milktubs'] = 'Dashboard/milktubs'; //S4
$route['milkdispatch'] = 'Dashboard/milkdispatch'; //S3

$route['saveMilkreceipt'] = 'Dashboard/saveMilkreceipt';
$route['saveMilkIssue'] = 'Dashboard/saveMilkIssue';
$route['saveMilkTubs'] = 'Dashboard/saveMilkTubs';
$route['saveMilkdispatch'] = 'Dashboard/saveMilkdispatch'; 

$route['productsreceipt'] = 'Dashboard/productsreceipt';
$route['productsdispatch'] = 'Dashboard/productsdispatch';
$route['productsissue'] = 'Dashboard/productsissue';
$route['productstubs'] = 'Dashboard/productstubs';

$route['saveProductreceipt'] = 'Dashboard/saveProductreceipt';  
$route['saveProductIssue'] = 'Dashboard/saveProductIssue';  
$route['saveProductdispatch'] = 'Dashboard/saveProductdispatch'; 
$route['saveProductTubs'] = 'Dashboard/saveProductTubs'; 

//Reports
$route['milkreceipt-report'] = 'Dashboard/milkreceiptReport';
$route['milkissue-report'] = 'Dashboard/milkissueReport';  
$route['milktubs-report'] = 'Dashboard/milktubsReport'; 
$route['milkdispatch-report'] = 'Dashboard/milkdispatchReport'; 

$route['summaryreceipt-report'] = 'Dashboard/summaryreceiptReport';
$route['summaryissue-report'] = 'Dashboard/summaryissueReport';  
$route['summarytubs-report'] = 'Dashboard/summarytubsReport'; 
$route['summarydispatch-report'] = 'Dashboard/summarydispatchReport'; 

$route['productreceipt-report'] = 'Dashboard/productreceiptReport';
$route['productissue-report'] = 'Dashboard/productissueReport';  
$route['producttubs-report'] = 'Dashboard/producttubsReport'; 
$route['productdispatch-report'] = 'Dashboard/productdispatchReport';  

$route['summarympreceipt-report'] = 'Dashboard/summarympreceiptReport';
$route['summarympissue-report'] = 'Dashboard/summarympissueReport';  
$route['summarymptubs-report'] = 'Dashboard/summarymptubsReport'; 
$route['summarympdispatch-report'] = 'Dashboard/summarympdispatchReport'; 

$route['milk-stock-report'] = 'Dashboard/milkStockReport'; 
$route['product-stock-report'] = 'Dashboard/productStockReport'; 

//APIS
$route['get_batchs_by_itemcode'] = 'Dashboard/get_batchs_by_itemcode'; 

$route['itemMaster'] = 'Dashboard/itemMaster';  
$route['saveItemMaster'] = 'Dashboard/saveItemMaster';

$route['training/test-form'] = 'Home/testForm'; 
$route['training/insertTestForm'] = 'Home/insertTestForm'; 
$route['training/insertTestForm1'] = 'Home/insertTestForm1'; 
