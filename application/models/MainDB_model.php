<?php
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
	 
class MainDB_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
 
	public function fill_select_box($status)
	{
		$where = "";
		if($status == 0){
			$where = " WHERE igroup = '101' AND status=1 ORDER BY misno";
		}

		if($status == 1){
			$where = " WHERE igroup = '102' AND status=1 ORDER BY iname";
		} 
		$query = $this->db->query("SELECT * FROM item_master $where"); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
		$output = '';
		foreach($result as $row)
		{
			$iname = strtoupper($row["iname"]);
			$output .= '<option value="'.$row["id"].'" data-icode="'.$row["icod"].'" data-iname="'.$row["iname"].'" data-packqty="'.$row["packqty"].'">'.ucfirst($iname).'</option>';
		}
		return $output;
	}

	public function deleteRecord($table,$column,$value){
		$sql = "DELETE FROM $table WHERE $column=$value";
		$query = $this->db->query($sql);
	}

	public function get_Result($sql){
		$query = $this->db->query($sql); 
        $cnt = $query->num_rows();
        $result = $query->result_array();
        return $result;
	}

	public function get_Result_By_Column($sql,$column){ 
		$query = $this->db->query($sql); 
        $cnt = $query->num_rows();
        $result = $query->result_array();
        $fdata = rtrim($result[0][$column],',');
        return $fdata;
	}

	public function checkMilkReceipt($shift,$rdate){ 
		$sql = "SELECT GROUP_CONCAT(milk_receipt_id) as milk_receipt_ids FROM milk_receipt WHERE rdate='$rdate' and shift='$shift'";
		$query = $this->db->query($sql); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
        return $result;
	}

	public function checkMilkReceiptItems($milk_receipt_id,$itemids){ 
		$sql = "SELECT GROUP_CONCAT(iname) as itemnames FROM item_master t1 INNER JOIN milk_receipt_items t2 ON t1.id=t2.item_id WHERE t2.milk_receipt_id IN($milk_receipt_id) AND t2.item_id IN($itemids)"; 
		$query = $this->db->query($sql); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
        return $result;
	}

	public function checkProductReceipt($shift,$rdate){ 
		$sql = "SELECT GROUP_CONCAT(product_receipt_id) as product_receipt_ids FROM products_receipt WHERE rdate='$rdate' and shift='$shift'";
		$query = $this->db->query($sql); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
        return $result;
	}

	public function checkProductReceiptItems($product_receipt_id,$itemids){ 
		$sql = "SELECT GROUP_CONCAT(iname) as itemnames FROM item_master t1 INNER JOIN products_receipt_items t2 ON t1.id=t2.item_id WHERE t2.product_receipt_id IN($product_receipt_id) AND t2.item_id IN($itemids)"; 
		$query = $this->db->query($sql); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
        return $result;
	}

	public function checkMilkIssue($shift,$rdate,$issue_type){ 
		$sql = "SELECT GROUP_CONCAT(milk_issue_id) as milk_issue_ids FROM milk_issue WHERE rdate='$rdate' and shift='$shift' AND issue_type='$issue_type'"; 
		$query = $this->db->query($sql); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
        return $result;
	}

	public function checkMilkIssueItems($milk_issue_id,$itemids){ 
		$sql = "SELECT GROUP_CONCAT(iname) as itemnames FROM item_master t1 INNER JOIN milk_issue_items t2 ON t1.id=t2.item_id WHERE t2.milk_issue_id IN($milk_issue_id) AND t2.item_id IN($itemids)"; 
		$query = $this->db->query($sql); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
        return $result;
	}

	public function checkProductIssue($shift,$rdate,$issue_type){ 
		$sql = "SELECT GROUP_CONCAT(product_issue_id) as product_issue_ids FROM products_issue WHERE rdate='$rdate' and shift='$shift' AND issue_type='$issue_type'"; 
		$query = $this->db->query($sql); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
        return $result;
	}

	public function checkProductIssueItems($product_issue_id,$itemids){ 
		$sql = "SELECT GROUP_CONCAT(iname) as itemnames FROM item_master t1 INNER JOIN products_issue_items t2 ON t1.id=t2.item_id WHERE t2.product_issue_id IN($product_issue_id) AND t2.item_id IN($itemids)"; 
		$query = $this->db->query($sql); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
        return $result;
	}

	public function get_batchs_by_itemcode($item_code,$type = NULL)
	{ 
		if($type){
			$table = "products_receipt_items";
		}else{
			$table = "milk_receipt_items";
		}
		$query = $this->db->query("select distinct batch_no from $table WHERE item_code='$item_code'"); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
		$output = '';
		foreach($result as $row)
		{ 
			$output .= '<option value="'.$row["batch_no"].'">'.$row["batch_no"].'</option>';
		}
		return $output;
	}

	public function get_milk_results($rdate,$shift,$rcode){
	  	$response = array();  
	    $date = date_create($rdate);
		$rdate = date_format($date,"Y-m-d"); 
	    $rdate = str_replace("-", "", $rdate);	 
		$data = array("Date" => $rdate,"Shift"=>$shift,"Route" => $rcode);
		$postdata = json_encode($data);
		$url = "http://115.243.13.124/REGI_FG_PP/REGIFGPP.asmx/GetMilkLoading?getDocumentDetails=$postdata";
		$headers = [];  
		$ch = curl_init($url); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		if(curl_exec($ch) === false)
		{
		    echo 'Curl error: ' . curl_error($ch);
		}
		curl_close($ch); 
		return json_decode($result);	
  	} 

  	public function get_product_results($rdate,$shift,$rcode){
	  	$response = array();  
	    $date = date_create($rdate);
		$rdate = date_format($date,"Y-m-d"); 
	    $rdate = str_replace("-", "", $rdate);	 
	    if($shift == "AM" && $rcode <=50){
	    	$shift = "AMB";
	    }
	    if($shift == "AM" && $rcode >50){
	    	$shift = "AMBR";
	    }
	    if($shift == "PM" && $rcode <=50){
	    	$shift = "PMB";
	    }
	    if($shift == "PM" && $rcode >50){
	    	$shift = "PMBR";
	    }
	    $headers = []; 
		$data = array("Date" => $rdate,"Shift"=>$shift,"Route" => $rcode);
		$postdata = json_encode($data); 
		$url = "http://115.243.13.124/REGI_FG_PP/REGIFGPP.asmx/GetProductsLodaing?getDocumentDetails=$postdata";  
		$ch = curl_init($url); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		if(curl_exec($ch) === false)
		{
		    echo 'Curl error: ' . curl_error($ch);
		}
		curl_close($ch); 
		return json_decode($result);	
  	} 

	public function getUserInfo($id) {
		$sql = "select * from users where user_id=?";
		$query = $this->db->query($sql,$id);
            return $query->result_array();
	}

	public function checkUser($ph,$pw) {
		$sql = "select * from users WHERE (mobile ='$ph' or email='$ph') AND password='$pw' AND user_status!=2"; 	
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		if($cnt==0){
			return 'no_user';
		}else{
			$row = $query->result_array();
			return $row[0]['user_id'];
		}		
	}

	public function checkDispatchRecordCount($mddata) {
		$rdate = $this->dateFormatChange($mddata['rdate'],1);
		//$sql = "select * from milk_dispatch WHERE rdate ='".$rdate."' and shift='".$mddata['shift']."' AND route='".$mddata['route']."' AND vehicle_no='".$mddata['vehicle_no']."' ORDER BY milk_dispatch_id desc";	
		$sql = "select * from milk_dispatch WHERE rdate ='".$rdate."' and shift='".$mddata['shift']."' AND route='".$mddata['route']."' ORDER BY milk_dispatch_id desc";	
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		return $query->result_array();		
	}

	public function checkDispatchItemsCount($milk_dispatch_id) {
		$sql = "select * from milk_dispatch_items WHERE milk_dispatch_id=$milk_dispatch_id";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		return $query->result_array();		
	}

	public function checkMilkTubsRecordCount($mddata) {
		$rdate = $this->dateFormatChange($mddata['rdate'],1);
		//$sql = "select * from milk_tubs WHERE rdate ='".$rdate."' and shift='".$mddata['shift']."' AND route='".$mddata['route']."' AND vehicle_no='".$mddata['vehicle_no']."' ORDER BY milk_tub_id desc";	
		$sql = "select * from milk_tubs WHERE rdate ='".$rdate."' and shift='".$mddata['shift']."' AND route='".$mddata['route']."' ORDER BY milk_tub_id desc";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		return $query->result_array();		
	}

	public function checkMilkTubsItemsCount($milk_tub_id) {
		$sql = "select * from milk_tubs_items WHERE milk_tub_id=$milk_tub_id";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		return $query->result_array();		
	}

	public function checkProductubsRecordCount($mddata) {
		$rdate = $this->dateFormatChange($mddata['rdate'],1);
		$sql = "select * from products_tubs WHERE rdate ='".$rdate."' and shift='".$mddata['shift']."' AND route='".$mddata['route']."' AND vehicle_no='".$mddata['vehicle_no']."' ORDER BY product_tub_id desc";	
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		return $query->result_array();		
	}

	public function checkProductTubsItemsCount($product_tub_id ) {
		$sql = "select * from products_tubs_items WHERE product_tub_id =$product_tub_id ";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		return $query->result_array();		
	}

	public function checkProductDispatchRecordCount($mddata) {
		$rdate = $this->dateFormatChange($mddata['rdate'],1);
		$sql = "select * from products_dispatch WHERE rdate ='".$rdate."' and shift='".$mddata['shift']."' AND route='".$mddata['route']."' AND vehicle_no='".$mddata['vehicle_no']."' ORDER BY product_dispatch_id desc";	
		$query = $this->db->query($sql);
		$cnt = $query->num_rows();
		return $query->result_array();		
	}

	public function checkProductDispatchItemsCount($product_dispatch_id) {
		$sql = "select * from products_dispatch_items WHERE product_dispatch_id=$product_dispatch_id";
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		return $query->result_array();		
	}

	public function getBatchs($item_code = NULL){ 
		if($item_code){
			$sql="select distinct batch_no from milk_receipt_items WHERE item_code LIKE '$item_code'";
		}else{
			$sql="select distinct batch_no from milk_receipt_items";
		}
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		if($cnt==0){
			return 'no_data';
		}else{
			$row = $query->result_array();
			return $row;
		}	
	}

	public function getProductBatchs($item_code = NULL){ 
		if($item_code){
			$sql="select distinct batch_no from products_receipt_items WHERE item_code LIKE '$item_code'";
		}else{
			$sql="select distinct batch_no from products_receipt_items";
		}
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		if($cnt==0){
			return 'no_data';
		}else{
			$row = $query->result_array();
			return $row;
		}	
	}

	public function getRoutes($flag){ 
		if($flag == 1){
			$sql="select distinct route from products_dispatch";
		}else{
			$sql="select distinct route from milk_dispatch";
		}
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		if($cnt==0){
			return 'no_data';
		}else{
			$row = $query->result_array();
			return $row;
		}	
	}

	public function getVehicles($flag){ 
		if($flag == 1){
			$sql="select distinct vehicle_no from products_dispatch";
		}else{
			$sql="select distinct vehicle_no from milk_dispatch";
		}
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		if($cnt==0){
			return 'no_data';
		}else{
			$row = $query->result_array();
			return $row;
		}	
	}

	public function getTubRoutes($flag){ 
		if($flag == 1){
			$sql="select distinct route from products_tubs";
		}else{
			$sql="select distinct route from milk_tubs";
		}
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		if($cnt==0){
			return 'no_data';
		}else{
			$row = $query->result_array();
			return $row;
		}	
	}

	public function getTubVehicles($flag){ 
		if($flag == 1){
			$sql="select distinct vehicle_no from products_tubs";
		}else{
			$sql="select distinct vehicle_no from milk_tubs";
		}
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		if($cnt==0){
			return 'no_data';
		}else{
			$row = $query->result_array();
			return $row;
		}	
	}

	public function getDispatchRoutes($flag){ 
		if($flag == 1){
			$sql="select distinct route from products_dispatch";
		}else{
			$sql="select distinct route from milk_dispatch";
		}
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		if($cnt==0){
			return 'no_data';
		}else{
			$row = $query->result_array();
			return $row;
		}	
	}

	public function getDispatchRoutesWithNames($flag){ 
		if($flag == 1){
			$sql="select distinct t1.route,t2.rname from products_dispatch t1 INNER JOIN routes_master t2 ON t1.route=t2.rcode";
		}else{
			$sql="select distinct t1.route,t2.rname from milk_dispatch t1 INNER JOIN routes_master t2 ON t1.route=t2.rcode";
		}
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		if($cnt==0){
			return 'no_data';
		}else{
			$row = $query->result_array();
			return $row;
		}	
	}

	public function getDispatchVehicles($flag){ 
		if($flag == 1){
			$sql="select distinct vehicle_no from products_dispatch";
		}else{
			$sql="select distinct vehicle_no from milk_dispatch";
		}
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		if($cnt==0){
			return 'no_data';
		}else{
			$row = $query->result_array();
			return $row;
		}	
	}

	public function getItemsMaster($status = NULL){ 
		$where = "";
		$where = " WHERE igroup = '101' AND status=1 ORDER BY misno";
		if($status == 1){
			$where = " WHERE igroup = '102' AND status=1 ORDER BY iname";
		}  
		$query = $this->db->query("SELECT * FROM item_master $where "); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
        return $result;
	}

	public function getRoutesMaster($status = NULL){  
		$query = $this->db->query("SELECT DISTINCT rcode,rname FROM routes_master WHERE status=1"); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
        return $result;
	}

	public function getRouteInfo($rcode){  
		$query = $this->db->query("SELECT * FROM routes_master WHERE rcode=$rcode"); 
        $cnt = $query->num_rows();
        $result = $query->result_array(); 
        return $result;
	}

	public function getMilkDispatchItems($rdate,$shift,$route,$vehicle_no){ 
		$rdate = $this->dateFormatChange($rdate,1);
		$sql = "select * from milk_dispatch_items t1 LEFT JOIN milk_dispatch t2 ON t1.milk_dispatch_id=t2.milk_dispatch_id WHERE t2.rdate = '$rdate' AND t2.shift='$shift' AND t2.route='$route' AND t2.vehicle_no='$vehicle_no'"; 	 
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		if($cnt==0){
			return 'no_data';
		}else{
			$row = $query->result_array();
			return $row;
		}	
	}

	public function getProductDispatchItems($rdate,$shift,$route,$vehicle_no){ 
		$rdate = $this->dateFormatChange($rdate,1);
		//$sql = "select * from products_dispatch_items t1 LEFT JOIN products_dispatch t2 ON t1.product_dispatch_id=t2.product_dispatch_id WHERE t2.rdate = '$rdate' AND t2.shift='$shift' AND t2.route='$route' AND t2.vehicle_no='$vehicle_no' AND t1.dis_total_tubs>0";
		$sql = "select * from products_dispatch_items t1 LEFT JOIN products_dispatch t2 ON t1.product_dispatch_id=t2.product_dispatch_id LEFT JOIN item_master t3 ON t1.item_code=t3.icod WHERE t2.rdate = '$rdate' AND t2.shift='$shift' AND t2.route='$route' AND t2.vehicle_no='$vehicle_no' AND t1.dis_total_tubs>0"; 	 
		$query = $this->db->query($sql);
		$cnt = $query->num_rows(); 
		if($cnt==0){
			return 'no_data';
		}else{
			$row = $query->result_array();
			return $row;
		}	
	}

	public function updateUserPassword($data,$user_id) {
		$this->db->where('user_id',$user_id);
		$this->db->update('users',$data);
		if($this->db->affected_rows() == 1) {
			return 1;
		} else {
			return 0;	
		}
	}

	public function updateProfileDetails($data,$user_id) { 
        $this->db->where('user_id',$user_id);
        $this->db->update('users',$data);
        if($this->db->affected_rows() == 1) {
            return 1;
        } else {
            return 0;   
        }
    }

	public function dateFormatChange($date,$type = NULL){
		$cdate1=date_create($date);
        $cdate = date_format($cdate1,"d-m-Y"); 
        if($type == 1){ //1 = US
        	 $cdate = date_format($cdate1,"Y-m-d"); 
        }
        return $cdate;
	}

	public function numDifference($num1,$num2){
		$diff = (double) $num1 - (double) $num2;
		return $diff;
	} 

	public function noDecimals($num1){
		$num1 = (float) $num1;
		$num1 = number_format($num1, 0, '.', '');  
		return $num1;
	}

	public function oneDecimals($num1){
		$num1 = (float) $num1;
		$num1 = number_format($num1, 1, '.', '');  
		return $num1;
	}

	public function cardBgColor($i){
		if($i == 0){
			$bgcolor = "bg-danger";
		}else if($i == 1){
			$bgcolor = "bg-warning";
		}else if($i == 2){
			$bgcolor = "bg-success";
		}else if($i == 3){
			$bgcolor = "bg-primary";
		}else if($i == 4){
			$bgcolor = "bg-info";
		}else if($i == 5){
			$bgcolor = "bg-light";
		}else if($i == 6){
			$bgcolor = "bg-secondary";
		}else if($i == 7){
			$bgcolor = "bg-gray";
		}else{
			$bgcolor = "bg-white";
		}
		return $bgcolor;
	}

	public function insertContactData($arr, $arr1 = NULL){
		 $this->db->insert('contact_form',$arr);
	}

	public function getContacts(){ 
		$query = $this->db->query("SELECT * FROM contact_form WHERE id IN(10,11) ORDER BY id DESC");  
        return $query->result_array();
	}
}