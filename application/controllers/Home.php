<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller { 

	public function __construct() {
        parent::__construct();
        if(!empty($this->session->userdata('user_id'))){
    		redirect(base_url('dashboard'), 'Location');
    	}
		$this->load->model('MainDB_model', 'mainModel');
    } 

	public function index(){ 
		$this->load->view('login'); 
	}  

	public function loginAction() {
		$response = array();
		$mobile = $this->input->post('emailMobile');
		$password = md5($this->input->post('password'));
		$checkUser = $this->mainModel->checkUser($mobile,$password);		 
		$userinfo = $this->mainModel->getUserInfo($checkUser);	 
		if($checkUser=='no_user'){
			$message= "Please Check Credentials";
		}else{			
   			if($userinfo[0]['user_status'] == 1){ 
   				$this->session->set_userdata('user_type', $userinfo[0]['user_type']);
				$this->session->set_userdata('user_id', $userinfo[0]['user_id']); 
				$this->session->set_userdata('name', $userinfo[0]['first_name']." ".$userinfo[0]['last_name']); 
	   			$message = "success";
			}else if($userinfo[0]['user_status'] == 0){
				$message= "Your account is inactive";
			}else if($userinfo[0]['user_status'] == 2){
				$message= "Your account is disabled";
			} 
		}
		echo $message;
	}

	public function testForm(){
		$data['contacts_data'] = $this->mainModel->getContacts();
		$this->load->view('test-form',$data);
	}

	public function insertTestForm(){
		$name  = $this->input->post('fname');
		$email = $this->input->post('email');
		$mobile = $this->input->post('mobile');
		$dob = $this->input->post('dob');
		$arr = array(
			"fname" => $name,
			"email" => $email,
			"mobile" => $mobile,
			"dob" => $dob,
		);
		$this->mainModel->insertContactData($arr,null);
		redirect(base_url('training/test-form'), 'Location');
	}


	public function insertTestForm1(){
		$fdata  = $this->input->post('fdata');  
		$this->mainModel->insertContactData($fdata,null);
		redirect(base_url('training/test-form'), 'Location');
	}
 
}
 