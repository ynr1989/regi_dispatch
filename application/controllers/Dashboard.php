<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller { 

	public function __construct() {
        parent::__construct();
        if(empty($this->session->userdata('user_id'))){
    		redirect(base_url(), 'Location');
    	}
		$this->load->model('MainDB_model', 'mainModel');
    } 

    public function dateFormatChange($date,$type = NULL){
		$cdate1=date_create($date);
        $cdate = date_format($cdate1,"d-m-Y"); 
        if($type == 1){ //1 = US
        	$cdate = date_format($cdate1,"Y-m-d"); 
        }
        return $cdate;
	}

    public function global_functions(){   
    	$data['user_type'] = $this->session->userdata('user_type');
    	$data['user_id'] = $this->session->userdata('user_id');  
    	$data['userInfo'] = $this->mainModel->getUserInfo($data['user_id']); 
		$data['name'] = $data['userInfo'][0]['first_name']." ".$data['userInfo'][0]['last_name'];  
		//echo strpos($_SERVER['HTTP_REFERER'] , 'rdate')."<br>";
		if(!isset($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'] , 'rdate') == ""){  
			//echo "ddd".$_SERVER['HTTP_REFERER']; die();
			$this->session->set_flashdata('message','');
			$this->session->set_flashdata('message1','');
		}		
    	return $data;
    } 

    public function callApi($url){
		$headers = [];  
		$ch = curl_init($url); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		if(curl_exec($ch) === false)
		{
		    echo 'Curl error: ' . curl_error($ch);
		}
		curl_close($ch); 
		return json_decode($result);
    }

	public function form1(){
		$this->load->view('templates/header');
		$this->load->view('form');
		$this->load->view('templates/footer');
	} 

	public function tabel(){
		$this->load->view('templates/header');
		$this->load->view('tabel');
		$this->load->view('templates/footer');
	} 

	public function index(){
		$data = $this->global_functions(); 

		//$rdate = "20211129";
		$rdate = date("dmY")+1;
		$shift = "AM";
		$request_data = array("Date" => $rdate,"Shift"=>$shift,"username" => API_USERNAME,"password" =>API_PAASSWORD);
		$postdata = json_encode($request_data);
		$data['milk_dashboard'] = $this->callApi(GET_MILK_DASHBOARD.$postdata);
		$data['curd_dashboard'] = $this->callApi(GET_CURD_DASHBOARD.$postdata);
		$this->load->view('templates/header',$data);
		$this->load->view('dashboard',$data);
		$this->load->view('templates/footer');
	} 

	public function milkreceipt(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('milkreceipt');
		$this->load->view('templates/footer');
	}

	public function itemMaster(){ 
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('itemMaster');
		$this->load->view('templates/footer');
	}

	public function saveItemMaster(){
		$data = $this->global_functions();
		if(isset($_POST["icod"]))
			{
			 $barray = array( 
			 	"icod" => $this->input->post("icod"),
			 	"iname" => $this->input->post("iname"),
			 	"misno" => $this->input->post("misno"),
			 	"igroup" => $this->input->post("igroup")
		 	);
			$insert = $this->db->insert('item_master',$barray); 
		}
		echo 'ok';
	}

	public function saveMilkreceipt(){
		$data = $this->global_functions();
		if(isset($_POST["item_id"]))
			{
			$shift = $this->input->post("shift");
			$rdate = $this->dateFormatChange($this->input->post("rdate"),1);
			 $barray = array(
			 	"rdate" => $rdate,
			 	"shift" => $shift,
			 	"remarks" => $this->input->post("bremarks"),
			 	"login_id" => $data["user_id"]
		 	);

			$checkRData = $this->mainModel->checkMilkReceipt($shift,$rdate);
			$item_id = $_POST["item_id"];
			$itemids = implode(',', $item_id);
			$itemids = rtrim($itemids, ", ");
			$checkRDataItems = 0;
			if(isset($checkRData[0]['milk_receipt_ids']) && $checkRData[0]['milk_receipt_ids']!=""){
				$milk_receipt_ids = $checkRData[0]['milk_receipt_ids'];
				$checkRDataItemsInfo = $this->mainModel->checkMilkReceiptItems($milk_receipt_ids,$itemids);
				
				if(isset($checkRDataItemsInfo[0]['itemnames']) && $checkRDataItemsInfo[0]['itemnames']!=""){
					$inames = implode(',',array_unique(explode(',', $checkRDataItemsInfo[0]['itemnames'])));
					$checkRDataItems = $inames;
				}
			}

			if($checkRDataItems == 0){
				 $this->db->insert('milk_receipt',$barray);
	             $milk_receipt_id = $this->db->insert_id(); 
				 $item_code = $_POST["item_code"];
				 $item_name = $_POST["item_name"];
				 $batch_no = $_POST["batch_no"];
				 $item_id = $_POST["item_id"];
				 $qty = $_POST["qty"];
				 $remarks = $_POST["remarks"];
				 $ninsert=0;
				 $query = '';
				 for($count = 0; $count<count($item_id); $count++)
				 { 
					if($item_id[$count] != '' && $item_code[$count] != '' && $qty[$count] != '')
					{
						$barray = array(
							"milk_receipt_id" => $milk_receipt_id,
							"item_id" => $item_id[$count],
							"item_code" => $item_code[$count],
							"batch_no" => $batch_no[$count],
							"qty" => $qty[$count],
							"remarks" => $remarks[$count],
							"line_no" => $count
						);
						$query = $this->db->insert('milk_receipt_items',$barray);
					}else{
						if($item_id[$count] != '' && $item_code[$count] != ''){							
							$ninsert =  $ninsert+1;
						}
					}
				 }
				 if($ninsert > 0){
				 	$this->mainModel->deleteRecord("milk_receipt",'milk_receipt_id',$milk_receipt_id);
				 	$this->mainModel->deleteRecord("milk_receipt_items",'milk_receipt_id',$milk_receipt_id);
				 	echo "Please enter qty.";
				 }else{
				 	echo 'ok';
				 }
				 
			}else{
				echo "$checkRDataItems already added.";
			}
		}
	}

	public function saveProductreceipt(){
		$data = $this->global_functions();
		if(isset($_POST["item_id"]))
			{
			$shift = $this->input->post("shift");
			$rdate = $this->dateFormatChange($this->input->post("rdate"),1);
			 $barray = array(
			 	"rdate" => $rdate,
			 	"shift" => $shift,
			 	"remarks" => $this->input->post("bremarks"),
			 	"login_id" => $data["user_id"]
		 	);

			 $checkRData = $this->mainModel->checkProductReceipt($shift,$rdate);
			$item_id = $_POST["item_id"];
			$itemids = implode(',', $item_id);
			$itemids = rtrim($itemids, ", ");
			$checkRDataItems = 0;
			if(isset($checkRData[0]['product_receipt_ids']) && $checkRData[0]['product_receipt_ids']!=""){
				$product_receipt_ids = $checkRData[0]['product_receipt_ids'];
				if($product_receipt_ids!=""){
					$checkRDataItemsInfo = $this->mainModel->checkProductReceiptItems($product_receipt_ids,$itemids);				
					if(isset($checkRDataItemsInfo[0]['itemnames']) && $checkRDataItemsInfo[0]['itemnames']!=""){
						$inames = implode(',',array_unique(explode(',', $checkRDataItemsInfo[0]['itemnames'])));
						$checkRDataItems = $inames;
					}
				}				
			}

			if($checkRDataItems == 0){
			 $this->db->insert('products_receipt',$barray);
             $product_receipt_id = $this->db->insert_id();
			  
			 $item_id = $_POST["item_id"];
			 $item_code = $_POST["item_code"];
			 $item_name = $_POST["item_name"];
			 $batch_no = $_POST["batch_no"];
			 $qty = $_POST["qty"];
			 $remarks = $_POST["remarks"];
			 
			 $query = '';
			 for($count = 0; $count<count($item_id); $count++)
			 { 
				if($item_id[$count] != '' && $item_code[$count] != '' && $qty[$count] != '')
				{
					$barray = array(
						"product_receipt_id" => $product_receipt_id,
						"item_id" => $item_id[$count],
						"item_code" => $item_code[$count],
						"batch_no" => $batch_no[$count],
						"qty" => $qty[$count],
						"remarks" => $remarks[$count],
						"line_no" => $count
					);
					$query = $this->db->insert('products_receipt_items',$barray);
				}else{
					if($item_id[$count] != '' && $item_code[$count] != ''){							
						$ninsert =  $ninsert+1;
					}
				}
			 }
			 if($ninsert > 0){
			 	$this->mainModel->deleteRecord("products_receipt",'product_receipt_id',$product_receipt_id);
			 	$this->mainModel->deleteRecord("products_receipt_items",'product_receipt_id',$product_receipt_id);
			 	echo "Please enter qty.";
			 }else{
			 	echo 'ok';
			 }
			 }else{
				echo "$checkRDataItems already added.";
			}
		}
	}

	public function saveProductIssue(){
		$data = $this->global_functions();
		if(isset($_POST["item_id"]))
			{
			$issue_type = $this->input->post("issue_type");
			$shift = $this->input->post("shift");
			$rdate = $this->dateFormatChange($this->input->post("rdate"),1);
			 $barray = array(
			 	"rdate" => $rdate,
			 	"shift" => $shift,
			 	"issue_type" => $issue_type,
			 	"remarks" => $this->input->post("bremarks"),
			 	"login_id" => $data["user_id"]
		 	);

			$checkRData = $this->mainModel->checkProductIssue($shift,$rdate,$issue_type);
			$item_id = $_POST["item_id"];
			$itemids = implode(',', $item_id);
			$itemids = rtrim($itemids, ", ");
			$checkRDataItems = 0;
			if(isset($checkRData[0]['product_issue_ids']) && $checkRData[0]['product_issue_ids']!=""){
				$product_issue_ids = $checkRData[0]['product_issue_ids'];
				$checkRDataItemsInfo = $this->mainModel->checkProductIssueItems($product_issue_ids,$itemids);		
				if(isset($checkRDataItemsInfo[0]['itemnames']) && $checkRDataItemsInfo[0]['itemnames']!=""){
					$inames = implode(',',array_unique(explode(',', $checkRDataItemsInfo[0]['itemnames'])));
					$checkRDataItems = $inames;
				}
			}

			if($checkRDataItems == 0){
			 $this->db->insert('products_issue',$barray);
             $product_issue_id = $this->db->insert_id();
			  
			 $item_id = $_POST["item_id"];
			 $item_code = $_POST["item_code"];
			 $item_name = $_POST["item_name"];
			 $batch_no = $_POST["batch_no"];
			 $qty = $_POST["qty"];
			 $remarks = $_POST["remarks"];
			 $tubs_baskets = $_POST["tubs_baskets"];
			 $ninsert = 0;
			 $query = '';
			 for($count = 0; $count<count($item_id); $count++)
			 { 
				if($item_id[$count] != '' && $item_code[$count] != '' && $batch_no[$count] != '' && $qty[$count] != '')
				{
					$barray = array(
						"product_issue_id" => $product_issue_id,
						"item_id" => $item_id[$count],
						"item_code" => $item_code[$count],
						"batch_no" => $batch_no[$count],
						"qty" => $qty[$count],
						"remarks" => $remarks[$count], 
						"tubs_baskets" => $tubs_baskets[$count],
						"line_no" => $count
					);
					$query = $this->db->insert('products_issue_items',$barray);
				}else{
					if($item_id[$count] != '' && $item_code[$count] != ''){							
						$ninsert =  $ninsert+1;
					}
				}
			 }
			 if($ninsert > 0){
			 	$this->mainModel->deleteRecord("products_issue",'product_issue_id',$product_issue_id);
			 	$this->mainModel->deleteRecord("products_issue_items",'product_issue_id',$product_issue_id);
			 	echo "Please enter qty.";
			 }else{
			 	echo 'ok';
			 }
			}else{
				echo "$checkRDataItems already added.";
			}
		}
	}

	public function saveMilkIssue(){
		$data = $this->global_functions();
		if(isset($_POST["item_id"]))
			{
			$issue_type = $this->input->post("issue_type");
			$shift = $this->input->post("shift");
			$rdate = $this->dateFormatChange($this->input->post("rdate"),1);
			 $barray = array(
			 	"rdate" => $rdate,
			 	"shift" => $shift,
			 	"issue_type" => $issue_type,
			 	"remarks" => $this->input->post("bremarks"),
			 	"login_id" => $data["user_id"]
		 	);

			$checkRData = $this->mainModel->checkMilkIssue($shift,$rdate,$issue_type);
			$item_id = $_POST["item_id"];
			$itemids = implode(',', $item_id);
			$itemids = rtrim($itemids, ", ");
			$checkRDataItems = 0;
			if(isset($checkRData[0]['milk_issue_ids']) && $checkRData[0]['milk_issue_ids']!=""){
				$milk_issue_ids = $checkRData[0]['milk_issue_ids'];
				$checkRDataItemsInfo = $this->mainModel->checkMilkIssueItems($milk_issue_ids,$itemids);		
				if(isset($checkRDataItemsInfo[0]['itemnames']) && $checkRDataItemsInfo[0]['itemnames']!=""){
					$inames = implode(',',array_unique(explode(',', $checkRDataItemsInfo[0]['itemnames'])));
					$checkRDataItems = $inames;
				}
			}

			if($checkRDataItems == 0){
				 $this->db->insert('milk_issue',$barray);
	             $milk_issue_id = $this->db->insert_id();
				  
				 $item_id = $_POST["item_id"];
				 $item_code = $_POST["item_code"];
				 $item_name = $_POST["item_name"];
				 $batch_no = $_POST["batch_no"];
				 $qty = $_POST["qty"];
				 $remarks = $_POST["remarks"];
				 $tubs_baskets = $_POST["tubs_baskets"];
				 
				 $query = '';
				 for($count = 0; $count<count($item_id); $count++)
				 { 
					if($item_id[$count] != '' && $item_code[$count] != '' && $qty[$count] != '')
					{
						$barray = array(
							"milk_issue_id" => $milk_issue_id,
							"item_id" => $item_id[$count],
							"item_code" => $item_code[$count],
							"batch_no" => $batch_no[$count],
							"qty" => $qty[$count],
							"remarks" => $remarks[$count], 
							"tubs_baskets" => $tubs_baskets[$count],
							"line_no" => $count
						);
						$query = $this->db->insert('milk_issue_items',$barray);
					}else{
						if($item_id[$count] != '' && $item_code[$count] != ''){							
							$ninsert =  $ninsert+1;
						}
					}
				 }
				 if($ninsert > 0){
				 	$this->mainModel->deleteRecord("milk_issue",'milk_issue_id',$milk_issue_id);
				 	$this->mainModel->deleteRecord("milk_issue_items",'milk_issue_id',$milk_issue_id);
				 	echo "Please enter qty.";
				 }else{
				 	echo 'ok';
				 }
			}else{
				echo "$checkRDataItems already added.";
			}
		}
	}

	public function saveMilkTubs(){
		$data = $this->global_functions();
		$mddata = $this->input->post("mddata");
		$ditems = $this->input->post('ditems');
		$data_count = 0;
		$checkCount = $this->mainModel->checkMilkTubsRecordCount($mddata);
		if(count($checkCount)>0){
			$data_count = $this->mainModel->checkMilkTubsItemsCount($checkCount[0]['milk_dispatch_id']);
			$data_count = count($data_count);
			$icount=0;
			for($i=0;$i<count($ditems);$i++){
	        	if($ditems[$i]['dis_sachet_qty'] > 0 || $ditems[$i]['dis_bulk_qty'] > 0){ 
	        		$icount++;
	        	}
        	}
		}   
		if(count($checkCount) == 0 || (count($checkCount) > 0 && $data_count!=$icount)){
			$mddata['login_id'] = $data["user_id"];
			$mddata['rdate'] = $this->dateFormatChange($mddata["rdate"],1); 
			$this->db->insert('milk_tubs',$mddata);
		    $milk_tub_id = $this->db->insert_id();
   
		    for($i=0;$i<count($ditems);$i++){
		    	$ditems[$i]['milk_tub_id'] = $milk_tub_id;  
		    	$this->db->insert('milk_tubs_items',$ditems[$i]);
		    }  
		    $this->session->set_flashdata('message', 'Success data saved.');
	    }else{
			$this->session->set_flashdata('message1', 'Data already submitted.'); 
			redirect($_SERVER['HTTP_REFERER']);
		} 
       redirect(base_url('milktubs'));
	}

	public function saveProductTubs(){
		$data = $this->global_functions();
		$mddata = $this->input->post("mddata"); 
		$ditems = $this->input->post('ditems');
		$data_count = 0;
		$checkCount = $this->mainModel->checkProductubsRecordCount($mddata);
		if(count($checkCount)>0){
			$data_count = $this->mainModel->checkProductTubsItemsCount($checkCount[0]['milk_dispatch_id']);
			$data_count = count($data_count);
			$icount=0;
			for($i=0;$i<count($ditems);$i++){
	        	if($ditems[$i]['dis_sachet_qty'] > 0 || $ditems[$i]['dis_bulk_qty'] > 0){ 
	        		$icount++;
	        	}
        	}
		}   
		if(count($checkCount) == 0 || (count($checkCount) > 0 && $data_count!=$icount)){
			$mddata['login_id'] = $data["user_id"];
			$mddata['rdate'] = $this->dateFormatChange($mddata["rdate"],1); 
			$this->db->insert('products_tubs',$mddata);
	        $product_tub_id = $this->db->insert_id();
	  
	        for($i=0;$i<count($ditems);$i++){
	        	$ditems[$i]['product_tub_id'] = $product_tub_id;  
	        	$this->db->insert('products_tubs_items',$ditems[$i]);
	        }  
	        $this->session->set_flashdata('message', 'Success data saved.');
        }else{
			$this->session->set_flashdata('message1', 'Data already submitted.'); 
			redirect($_SERVER['HTTP_REFERER']);
		} 
       redirect(base_url('productstubs'));
	} 

	public function saveMilkdispatch(){
		$data = $this->global_functions();
		$mddata = $this->input->post("mddata");
		$ditems = $this->input->post('ditems');
		if(isset($ditems)){
			$data_count = 0;
			$checkCount = $this->mainModel->checkDispatchRecordCount($mddata);
			if(count($checkCount)>0){
				$data_count = $this->mainModel->checkDispatchItemsCount($checkCount[0]['milk_dispatch_id']);
				$data_count = count($data_count);
				$icount=0;
				for($i=0;$i<count($ditems);$i++){
		        	if($ditems[$i]['dis_sachet_qty'] > 0 || $ditems[$i]['dis_bulk_qty'] > 0){ 
		        		$icount++;
		        	}
	        	}
			}  

			//echo count($checkCount)." == 0 || ".count($checkCount)." > 0 && $data_count!=$icount"; die();
			if(count($checkCount) == 0 || (count($checkCount) > 0 && $data_count!=$icount)){
				$mddata['login_id'] = $data["user_id"];
				$mddata['vehicle_no'] = strtoupper(str_replace(" ","",$mddata["vehicle_no"]));
				$mddata['rdate'] = $this->dateFormatChange($mddata["rdate"],1); 
				$this->db->insert('milk_dispatch',$mddata);
		        $milk_dispatch_id = $this->db->insert_id(); 
		        $batch_no = $this->input->post('batch_no'); 
		         
		        for($i=0;$i<count($ditems);$i++){
		        	if($ditems[$i]['dis_sachet_qty'] > 0 || $ditems[$i]['dis_bulk_qty'] > 0){ 
			        	$ditems[$i]['milk_dispatch_id'] = $milk_dispatch_id; 
			        	$ditems[$i]['batch_no'] = isset($batch_no[$i])?implode(",",$batch_no[$i]):''; 
			        	$this->db->insert('milk_dispatch_items',$ditems[$i]);
		        	}
		        }  
		        $this->session->set_flashdata('message', 'Success data saved.');
			}else{
				$this->session->set_flashdata('message1', 'Data already submitted.'); 
				redirect($_SERVER['HTTP_REFERER']);
			} 
	        redirect(base_url('milkdispatch'));
    	}
	}


	public function saveProductdispatch(){
		$data = $this->global_functions();
		$mddata = $this->input->post("mddata");
		$ditems = $this->input->post('ditems');
		$data_count = 0;
		$checkCount = $this->mainModel->checkProductDispatchRecordCount($mddata);
		if(count($checkCount)>0){
			$data_count = $this->mainModel->checkProductDispatchItemsCount($checkCount[0]['product_dispatch_id']);
			$data_count = count($data_count);
			$icount=0;
			for($i=0;$i<count($ditems);$i++){
	        	if($ditems[$i]['dis_sachet_qty'] > 0 || $ditems[$i]['dis_bulk_qty'] > 0){ 
	        		$icount++;
	        	}
        	}
		}  
		//echo count($checkCount)." == 0 || ".count($checkCount)." > 0 && $data_count!=$icount"; die();
		if(count($checkCount) == 0 || (count($checkCount) > 0 && $data_count!=$icount)){			
			$mddata['login_id'] = $data["user_id"];
			$mddata['vehicle_no'] = strtoupper(str_replace(" ","",$mddata["vehicle_no"]));
			$mddata['rdate'] = $this->dateFormatChange($mddata["rdate"],1); 
			$this->db->insert('products_dispatch',$mddata);
	        $product_dispatch_id = $this->db->insert_id();

	        
	        $batch_no = $this->input->post('batch_no'); 
	         
	        for($i=0;$i<count($ditems);$i++){
	        	$ditems[$i]['product_dispatch_id'] = $product_dispatch_id; 
	        	$ditems[$i]['batch_no'] = isset($batch_no[$i])?implode(",",$batch_no[$i]):''; 
	        	$this->db->insert('products_dispatch_items',$ditems[$i]);
	        }  
	        $this->session->set_flashdata('message', 'Success data saved.');
		}else{
			$this->session->set_flashdata('message1', 'Data already submitted.'); 
			redirect($_SERVER['HTTP_REFERER']);
		}
       redirect(base_url('productsdispatch'));
	}
	
	public function milkdispatch(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('milkdispatch');
		$this->load->view('templates/footer');
	}
	
	public function milkissue(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('milkissue');
		$this->load->view('templates/footer');
	}
	
	public function milktubs(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('milktubs');
		$this->load->view('templates/footer');
	}
	
	public function productsreceipt(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('productsreceipt');
		$this->load->view('templates/footer');
	}
	
	public function productsdispatch(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);  
		$this->load->view('productsdispatch');
		$this->load->view('templates/footer');
	}
	
	public function productsissue(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('productsissue');
		$this->load->view('templates/footer');
	}
	
	public function productstubs(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('productstubs');
		$this->load->view('templates/footer');
	}

	public function updateProfile(){
		$data = $this->global_functions();		
		$this->load->view('templates/header',$data);
		$this->load->view('dashboard/update-profile',$data);
		$this->load->view('templates/footer');
	}

	public function profileSubmit() {
		$data = $this->global_functions();
		if($_FILES["profilePic"]["name"]!='') {
			$target_dir = "assets/profilePics/";
			$target_file = $target_dir . basename($_FILES["profilePic"]["name"]);
			$file_name = basename($_FILES["profilePic"]["name"]);
			move_uploaded_file($_FILES["profilePic"]["tmp_name"], $target_file);
		} else{
			$file_name = $data['userInfo'][0]['profilePic'];
		}
		$form_data = array(
			'first_name' => $this->input->post('firstName'),
			'last_name' => $this->input->post('lastName'),
			'email' => $this->input->post('email'),
			'mobile' => $this->input->post('mobile'),
			'profilePic' => $file_name 
		);
		
		$update = $this->mainModel->updateProfileDetails($form_data,$data['user_id']);
		$this->session->set_flashdata('message', 'Profile updated successfully');		
		redirect(base_url('update-profile'), 'Location');
	}

	public function changePassword(){
		$data = $this->global_functions();		 
		$this->load->view('templates/header',$data);
		$this->load->view('dashboard/change-password',$data); 
		$this->load->view('templates/footer');
	}

	public function submitChangePassword(){
		$data = $this->global_functions();	
		$password = $this->input->post('password'); 
		$confirm_password = $this->input->post('confirm_password');
		if($password==$confirm_password) {
			$pw = md5($password);
			$form_data = array(
			'password' => $pw,
			//'test_password' =>$password
			//'passwordFlag' =>'1'	
			);		

			if($data['user_type'] == 2){
				$update = $this->mainModel->updateBusinessPassword($form_data,$data['user_id']);
			}else{  
				$update = $this->mainModel->updateUserPassword($form_data,$data['user_id']);
			}
			$this->session->set_flashdata('messageStatus', 'success');
			$this->session->set_flashdata('message1', '');
		    $this->session->set_flashdata('message2', 'Password updated successfully');
			redirect(base_url('change-password'), 'Location');
		}else{
			$this->session->set_flashdata('message2', '');
		    $this->session->set_flashdata('message1', 'Confirm password wrong.');
			redirect(base_url('change-password'), 'Location');
		}
	} 

	public function milkreceiptReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/milkreceiptReport');
		$this->load->view('templates/footer');
	}

	public function summaryreceiptReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/summaryreceiptReport');
		$this->load->view('templates/footer');
	}

	public function milkissueReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/milkissueReport');
		$this->load->view('templates/footer');
	}
	
	public function summaryissueReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/summaryissueReport');
		$this->load->view('templates/footer');
	}

	public function milktubsReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/milktubsReport');
		$this->load->view('templates/footer');
	}
	
	public function summarytubsReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/summarytubsReport');
		$this->load->view('templates/footer');
	}

	public function milkdispatchReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/milkdispatchReport');
		$this->load->view('templates/footer');
	}

    public function summarydispatchReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/summarydispatchReport');
		$this->load->view('templates/footer');
	}
	
	public function productreceiptReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/productreceiptReport');
		$this->load->view('templates/footer');
	}
	
	public function summarympreceiptReport(){
		$data = $this->global_functions(); 
		$this->load->view('templates/header',$data);
		$this->load->view('reports/summarympreceiptReport');
		$this->load->view('templates/footer');
	}

	public function milkStockReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/milkStockReport');
		$this->load->view('templates/footer');
	}

	public function productStockReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/productStockReport');
		$this->load->view('templates/footer');
	}

	public function productissueReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/productissueReport');
		$this->load->view('templates/footer');
	}
	
	public function summarympissueReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/summarympissueReport');
		$this->load->view('templates/footer');
	}

	public function producttubsReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/producttubsReport');
		$this->load->view('templates/footer');
	}

    public function summarymptubsReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/summarymptubsReport');
		$this->load->view('templates/footer');
	}
	
	public function productdispatchReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/productdispatchReport');
		$this->load->view('templates/footer');
	}
	
	public function summarympdispatchReport(){
		$data = $this->global_functions();
		$this->load->view('templates/header',$data);
		$this->load->view('reports/summarympdispatchReport');
		$this->load->view('templates/footer');
	}

	//APIS
	public function get_batchs_by_itemcode(){
		$item_code = $this->input->post('item_code');
		$type = $this->input->post('type');
		echo $this->mainModel->get_batchs_by_itemcode($item_code,$type);
	}

	public function logout() {	
		$this->session->sess_destroy();
		redirect(base_url('/'), 'Location');
	}
}
